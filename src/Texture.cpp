/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief Texture.cpp
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-09-26
 */

#include "GLE/Texture.h"

#include "GLE/Utils/String.h"
#include "GLE/Utils/ByteArray.h"
#include "GLE/Utils/File.h"

#include <turbojpeg.h>
//#include <png.h>
#include <webp/decode.h>

#include <cstdio>
#include <cstring>

namespace GLE_NS {

Texture::Texture(const String& file, Type type)
  : GLObject(),
    _type(type),
    _width(0),
    _height(0)
{
	glGenTextures(1, &_id);

	ByteArray texels;
	ByteArray fileBuff = File::getContent(file);

	if (file.contains("."))
	{
		String ext = file.section(".", -1, -1).toAsciiLower();

		if (ext == "jpg" || ext == "jpeg")
			texels = fromJpeg(fileBuff);
		else if (ext == "png")
			texels = fromPng(fileBuff);
		else if (ext == "webp")
			texels = fromWebp(fileBuff);
		else
			throw 0xdeadbeef;
	}
	else
		throw 0xdeadbeef;

	bind();
	glTexParameteri(_type, GL_TEXTURE_BASE_LEVEL, 0);
	glTexParameteri(_type, GL_TEXTURE_MAX_LEVEL, 0);
	glTexParameteri(_type, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(_type, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(_type, GL_TEXTURE_MAX_ANISOTROPY_EXT,
	                       GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT);
	glTexImage2D(_type, 0, GL_RGBA, _width, _height, 0,
	             GL_RGBA, GL_UNSIGNED_BYTE, texels.data());
	glGenerateMipmap(_type);
}

ByteArray Texture::fromJpeg(ByteArray& datas)
{
	tjhandle decomp = tjInitDecompress();
	if (decomp == nullptr)
		throw 0xdeadbeef;

	int chrom;
	if (tjDecompressHeader2(decomp,
	                        reinterpret_cast<unsigned char*>(datas.data()),
	                        datas.size(), &_width, &_height, &chrom) < 0)
		throw 0xdeadbeef;

	std::size_t outSize = _width * _height * 4;
	ByteArray texels;
	texels.reserve(outSize);

	if (tjDecompress2(decomp, reinterpret_cast<unsigned char*>(datas.data()),
	                  datas.size(),
	                  reinterpret_cast<unsigned char*>(texels.data()),
	                  _width, 0, _height, TJPF_RGBA,
	                  TJFLAG_BOTTOMUP | TJFLAG_FASTDCT) < 0)
		throw 0xdeadbeef;

	tjDestroy(decomp);
	return texels;
}

ByteArray Texture::fromPng(const ByteArray&)
{
	return ByteArray();
}

ByteArray Texture::fromWebp(const ByteArray& datas)
{
	uint8_t* outBuff
	    = WebPDecodeRGBA(reinterpret_cast<const uint8_t*>(datas.data()),
	                     datas.size(), &_width, &_height);
	if (outBuff == nullptr)
		std::cerr << "FAILED" << std::endl;

	uint8_t* tmpBuff = new uint8_t[_width * 4];
	for (int y = 0, hh = _height / 2; y < hh; y++)
	{
		std::memcpy(tmpBuff,
		            outBuff + (y * _width * 4),
		            _width * 4);
		std::memcpy(outBuff + (y * _width * 4),
		            outBuff + ((_height - y - 1) * _width * 4),
		            _width * 4);
		std::memcpy(outBuff + ((_height - y - 1) * _width * 4),
		            tmpBuff,
		            _width * 4);
	}
	delete[] tmpBuff;

	ByteArray texels(reinterpret_cast<char*>(outBuff), _width * _height * 4);
	free(outBuff);
	return texels;
}

void Texture::bind()
{
	glBindTexture(_type, _id);
}

void Texture::bind(int index)
{
	glActiveTexture(GL_TEXTURE0 + index);
	bind();
}

} // ns GLE_NS
