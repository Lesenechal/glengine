/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @file Utils/File.cpp
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-04-01
 */

#include "GLE/Utils/File.h"
#include "GLE/Utils/ByteArray.h"

File::File(const String& path)
{
	_path = path;
}

void File::open(int mode)
{
	std::ios_base::openmode stdMode = static_cast<std::ios_base::openmode>(0);

	if (mode & ReadOnly)
		stdMode |= std::fstream::in;
	if (mode & WriteOnly)
		stdMode |= std::fstream::out;
	if (mode & Append)
		stdMode |= std::fstream::app;
	if (mode & Truncate)
		stdMode |= std::fstream::trunc;
	if (!(mode & Text))
		stdMode |= std::fstream::binary;

	std::fstream::open(_path.toCString(), stdMode);
}

void File::write(const ByteArray& datas)
{
	reinterpret_cast<std::ostream*>(this)->write(datas.data(), datas.size());
}

void File::close()
{
	std::fstream::close();
}

ByteArray File::getContent(const String& path)
{
	File file(path);
	file.open(ReadOnly);

	ByteArray r;

	while (!file.std::ios::eof())
	{
		char* buffer = new char[1024];
		file.std::fstream::read(buffer, 1024);
		r.append(buffer, file.std::fstream::gcount());
		delete[] buffer;
	}

	return r;
}
