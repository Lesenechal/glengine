/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @file Utils/StringList.cpp
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-03-28
 */

#include "GLE/Utils/StringList.h"

StringList::StringList()
  : List<String>()
{

}

StringList::StringList(const List<String>& list)
  : List<String>(list)
{ }

StringList::StringList(const std::initializer_list<String>& initList)
  : List<String>(initList)
{ }


String StringList::join(const String& delem) const
{
	String r;
	for (int i = 0; i < count(); i++)
		r += at(i) + (i < count() - 1 ? delem : "");
	return r;
}

String StringList::dump() const
{
	String r = String("String list (%1 items) [").format(count());
	for (int i = 0; i < count(); i++)
	{
		r += String("\"") + at(i) + "\"";
		if (i + 1 < count())
			r += ", ";
	}
	return r + "]";
}
