/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief Contexts/GLContext.cpp
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-04-01
 */

#include "GLE/Contexts/GLContext.h"
#include "GLE/Contexts/X11Context.h"
#include "GLE/Contexts/Win32Context.h"

#include "GLE/GLEngine.h"

namespace GLE_NS {

GLContext::GLContext()
  : _inited(false),
    _fullScreen(false),
    _borderLess(false),
    _cursorVisible(true),
    _cursorWarped(false),
    _doubleBuffering(true),
    _vSync(true),
    _isMaximized(false),
    _windowWidth(1024),
    _windowHeight(768),
    _windowX(0),
    _windowY(0),
    _windowTitle("GLEngine"),
    _glMajorVersion(3),
    _glMinorVersion(0),
    _compatibilityProfile(false),
    _keyRepeat(false)
{}

GLContext::~GLContext()
{

}


GLContext* GLContext::create()
{
#if GLE_WINDOW_SYSTEM == GLE_WIN32
	return new Win32Context;
#elif GLE_WINDOW_SYSTEM == GLE_CGL
	return new CGLContext;
#elif GLE_WINDOW_SYSTEM == GLE_X11
	return new X11Context;
#else
	return nullptr;
#endif
}

//----------------------------------------------------------------------------//

void GLContext::setDoubleBuffering(bool doubleBufferingEnabled)
{ _doubleBuffering = doubleBufferingEnabled; }

void GLContext::setVSync(bool vSync)
{ _vSync = vSync; }

void GLContext::setOpenGLVersion(int major, int minor)
{
	_glMajorVersion = major;
	_glMinorVersion = minor;
}

void GLContext::setMinimalOpenGLVersion(int major, int minor)
{
	_minGlMajorVersion = major;
	_minGlMinorVersion = minor;
}

void GLContext::showCursor()
{
	setCursorVisibility(true);
}

void GLContext::hideCursor()
{
	setCursorVisibility(false);
}

void GLContext::warpCursor()
{
	setCursorWarped(true);
}

void GLContext::unwarpCursor()
{
	setCursorWarped(false);
}

//----------------------------------------------------------------------------//

void GLContext::setCompatibilityProfile()
{ _compatibilityProfile = true; }

int GLContext::glMajorVersion() const
{ return _glMajorVersion; }

int GLContext::glMinorVersion() const
{ return _glMinorVersion; }

bool GLContext::isInitialized() const
{ return _inited; }

} // ns GLE_NS
