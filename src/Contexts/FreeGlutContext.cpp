/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief Contexts/FreeGlutContext.cpp
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-05-11
 */

#include "GLE/Contexts/FreeGlutContext.h"

#if 0

#include "OpenGL.h"

#include <GL/freeglut.h>
#include <GL/freeglut_ext.h>

namespace GLE_NS {

FreeGlutContext::FreeGlutContext()
{

}

FreeGlutContext::~FreeGlutContext()
{

}

void FreeGlutContext::init()
{
	int argc = 0, flags;

	glutInit(&argc, nullptr);

	flags  = GLUT_RGBA | GLUT_DEPTH;
	flags |= (_doubleBuffering ? GLUT_DOUBLE : GLUT_SINGLE);

	glutInitDisplayMode(flags);
	glutInitContextVersion(_glMajorVersion, _glMinorVersion);
	glutInitContextProfile(_compatibilityProfile ? GLUT_COMPATIBILITY_PROFILE
	                                             : GLUT_CORE_PROFILE);
	glutInitWindowSize(_windowWidth, _windowHeight);
	glutCreateWindow(_windowTitle);

	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
	{
		std::cerr << "GLEW: failed to initialize." << std::endl;
		exit(1);
	}

	_inited = true;

	std::cout << "Running on OpenGL " << glGetString(GL_VERSION)
	          << " with FreeGLUT" << std::endl;
}

void FreeGlutContext::swapBuffers()
{
	glutSwapBuffers();
}

} // ns GLE_NS

#endif // GLE_WINDOW_SYSTEM == GLE_FREEGLUT
