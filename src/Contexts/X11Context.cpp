/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief Contexts/X11Context.cpp
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-04-01
 */

#include "GLE/GLEngine.h"

#if GLE_WINDOW_SYSTEM == GLE_X11

#include "GLE/OpenGL.h"
#include "GLE/Contexts/X11Context.h"

#include "GLE/Events/WindowEvent.h"

#include "GLE/Utils/Map.h"

#include <iostream>
#include <cstring>
#include <string.h>

namespace GLE_NS {

Map<unsigned int, KeyEvent::Key> X11Context::_keyCodes;
Map<unsigned int, MouseButtonEvent::Button> X11Context::_mouseButtons;

X11Context::X11Context()
  : _initiating(false),
    _movedWindow(false),
    _minGlxMajorVersion(1),
    _minGlxMinorVersion(3),
    _display(nullptr),
    _window(0),
    _gc(nullptr),
    _visual(nullptr),
    _glxWindow(0)
{}

X11Context::~X11Context()
{
	if (_glxWindow != 0)
		glXDestroyWindow(_display, _glxWindow);
	if (_visual != nullptr)
		XFree(_visual);
	if (_gc != nullptr)
		XFreeGC(_display, _gc);
	if (_window != 0)
		XDestroyWindow(_display, _window);
	if (_display != nullptr)
		XCloseDisplay(_display);
}

static void fatal(const String& message)
{
	std::cerr << "Fatal error: GLX: " << message << std::endl;
	exit(1);
}

typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*, GLXFBConfig,
                                                     GLXContext, Bool,
                                                     const int*);

XIMStyle chooseBetterStyle(XIMStyle style1, XIMStyle style2)
{
	XIMStyle s, t;
	XIMStyle preedit = XIMPreeditArea | XIMPreeditCallbacks
	                   | XIMPreeditPosition | XIMPreeditNothing | XIMPreeditNone;
	XIMStyle status = XIMStatusArea | XIMStatusCallbacks
	                  | XIMStatusNothing | XIMStatusNone;
	if (style1 == 0)
		return style2;
	if (style2 == 0)
		return style1;
	if ((style1 & (preedit | status)) == (style2 & (preedit | status)))
		return style1;
	s = style1 & preedit;
	t = style2 & preedit;

	if (s != t)
	{
		if (s | t | XIMPreeditCallbacks)
			return s == XIMPreeditCallbacks ? style1 : style2;
		else if (s | t | XIMPreeditPosition)
			return s == XIMPreeditPosition ? style1 : style2;
		else if (s | t | XIMPreeditArea)
			return s == XIMPreeditArea ? style1 : style2;
		else if (s | t | XIMPreeditNothing)
		return s == XIMPreeditNothing ? style1 : style2;
	}
	else
	{
		s = style1 & status;
		t = style2 & status;
		if (s | t | XIMStatusCallbacks)
			return s == XIMStatusCallbacks ? style1 : style2;
		else if (s | t | XIMStatusArea)
			return s == XIMStatusArea ? style1 : style2;
		else if (s | t | XIMStatusNothing)
			return s == XIMStatusNothing ? style1 : style2;
	}
}

void X11Context::init()
{
	_initiating = true;

	// XInitThreads()? Nvidia proprietary driver
	_display = XOpenDisplay(nullptr);
	if (_display == nullptr)
		fatal("failed to open X display"); // TODO: throw exception

	if (glXQueryExtension(_display, nullptr, nullptr) == False)
		fatal("GLX not supported by the X server");

	int glxMajorVersion, glxMinorVersion;
	if (glXQueryVersion(_display, &glxMajorVersion, &glxMinorVersion) == False)
		fatal("failed to query GLX's version");

	if (glxMajorVersion < _minGlxMajorVersion
	    || glxMinorVersion < _minGlxMinorVersion)
	{
		fatal(String("GLX version (%1.%2) too old, expected at least version %3.%4")
		      .format({glxMajorVersion, glxMinorVersion,
		               _minGlxMajorVersion, _minGlxMinorVersion}));
	}

	glXChooseFBConfig = ext<PFNGLXCHOOSEFBCONFIGPROC>("glXChooseFBConfig");
	glXGetVisualFromFBConfig = ext<PFNGLXGETVISUALFROMFBCONFIGPROC>(
	                             "glXGetVisualFromFBConfig");
	glXCreateWindow = ext<PFNGLXCREATEWINDOWPROC>("glXCreateWindow");

	if (glXChooseFBConfig == nullptr
	    || glXGetVisualFromFBConfig == nullptr
	    || glXCreateWindow == nullptr)
		fatal("some basic GLX functions were not found.");

	int fbConfig[] = {
		GLX_RENDER_TYPE,   GLX_RGBA_BIT,
		GLX_X_RENDERABLE,  True,
		GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
		GLX_DOUBLEBUFFER,  _doubleBuffering ? True : False,
		GLX_DEPTH_SIZE,    24,
		GLX_RED_SIZE,      8,
		GLX_GREEN_SIZE,    8,
		GLX_BLUE_SIZE,     8,
	  GLX_SAMPLE_BUFFERS, 1,
	  GLX_SAMPLES,       8,
		0
	};
	int fbConfigsCount;
	GLXFBConfig* fbConfigs = glXChooseFBConfig(_display, DefaultScreen(_display),
	                                           fbConfig, &fbConfigsCount);
	if (fbConfigsCount == 0)
		fatal("there is no framebuffer configurations available for these options");
	std::cout << "Has " << fbConfigsCount << " FB configs." << std::endl;
	_visual = glXGetVisualFromFBConfig(_display, fbConfigs[0]);
	if (_visual == nullptr)
		fatal("could not retrieve the visual infos");
	std::cout << "[X11] Visual 0x" << std::hex << _visual->visualid << std::endl;

	_atomWinClose = XInternAtom(_display, "WM_DELETE_WINDOW", False);
	_atomWinName  = XInternAtom(_display, "_NET_WM_NAME", False);
	_atomState    = XInternAtom(_display, "_NET_WM_STATE", False);
	_atomStateFullscreen = XInternAtom(_display, "_NET_WM_STATE_FULLSCREEN",
	                                   False);
	_atomStateMaxiHoriz  = XInternAtom(_display, "_NET_WM_STATE_MAXIMIZED_HORZ",
	                                   False);
	_atomStateMaxiVert   = XInternAtom(_display, "_NET_WM_STATE_MAXIMIZED_VERT",
	                                   False);

	XSetWindowAttributes win;
	win.event_mask = StructureNotifyMask | ButtonPressMask | ButtonReleaseMask
	                 | KeyPressMask| KeyReleaseMask | PointerMotionMask;
	win.background_pixmap = None;
	win.background_pixel  = 0;
	win.border_pixel      = 0;
	win.colormap = XCreateColormap(_display, DefaultRootWindow(_display),
	                               _visual->visual, AllocNone);
	_window = XCreateWindow(_display, DefaultRootWindow(_display),
	                        10, 10, _windowWidth, _windowHeight,
	                        0, _visual->depth, InputOutput, _visual->visual,
	                        CWBackPixmap | CWBorderPixel
	                        | CWColormap | CWEventMask,
	                        &win);
	setWindowTitle(_windowTitle);
	setWindowBorderLess(_borderLess);
	XSelectInput(_display, _window, StructureNotifyMask | ButtonPressMask
	                                | ButtonReleaseMask | KeyPressMask
	                                | KeyReleaseMask | PointerMotionMask);
	XMapWindow(_display, _window);
	if (_movedWindow)
		setWindowPosition(_windowX, _windowY);
	if (_isMaximized)
		setWindowMaximized(_isMaximized);
	XUndefineCursor(_display, _window); // ?

	XSetWMProtocols(_display, _window, &_atomWinClose, 1);

	_gc = XCreateGC(_display, _window, 0, nullptr);

	initKeyCodes();
	XAutoRepeatOn(_display);
	_im = XOpenIM(_display, nullptr, nullptr, nullptr);
	XIMStyles* imSupportedStyles;
	XGetIMValues(_im, XNQueryInputStyle, &imSupportedStyles, nullptr);
	XIMStyle appSupportedStyles = XIMPreeditNone | XIMPreeditNothing
	                              | XIMPreeditArea | XIMStatusNone
	                              | XIMStatusNothing | XIMStatusArea;
	XIMStyle bestStyle = 0;
	for (int i = 0; i < imSupportedStyles->count_styles; i++)
	{
		XIMStyle style = imSupportedStyles->supported_styles[i];
		if ((style & appSupportedStyles) == style)
			bestStyle = chooseBetterStyle(style, bestStyle);
	}
	_ic = XCreateIC(_im, XNInputStyle, bestStyle,
	                     XNClientWindow, _window,
	                nullptr);

	while (true)
	{
		XEvent event;
		XNextEvent(_display, &event);
		if (event.type == MapNotify)
			break;
	}

	_glxWindow = glXCreateWindow(_display, fbConfigs[0], _window, nullptr);

	GLint glContextAttribs[] = {
		GLX_CONTEXT_MAJOR_VERSION_ARB, _glMajorVersion,
		GLX_CONTEXT_MINOR_VERSION_ARB, _glMinorVersion,
		GLX_CONTEXT_PROFILE_MASK_ARB,  _compatibilityProfile
		                                 ? GLX_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB
		                                 : GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
		0
	};

	glXCreateContextAttribsARB = ext<PFNGLXCREATECONTEXTATTRIBSARBPROC>(
	                               "glXCreateContextAttribsARB");
	if (glXCreateContextAttribsARB == nullptr)
	{
		fatal("failed to retrieve function `glXCreateContextAttribsARB' from "
		      "extension `GLX_ARB_create_context_profile'");
	}

	_glxContext = glXCreateContextAttribsARB(_display, fbConfigs[0], 0,
	                                         True, glContextAttribs);
	XFree(fbConfigs);
	if (_glxContext == nullptr)
		fatal("failed to create OpenGL context");

	glXMakeCurrent(_display, _glxWindow, _glxContext);

	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
	{
		std::cerr << "GLEW: failed to initialize." << std::endl;
		exit(1);
	}

	if (GLXEW_EXT_swap_control_tear)
	{
		std::cout << "GLXEW_EXT_swap_control_tear" << std::endl;
		glXSwapIntervalEXT(_display, _glxWindow, _vSync ? -1 : 0);
	}
	else if (GLXEW_EXT_swap_control)
	{
		std::cout << "GLXEW_EXT_swap_control" << std::endl;
		glXSwapIntervalEXT(_display, _glxWindow, _vSync ? 1 : 0);
	}
	else if (GLXEW_MESA_swap_control)
	{
		std::cout << "GLXEW_MESA_swap_control" << std::endl;
		glXSwapIntervalMESA(_vSync ? 1 : 0);
	}
	else if (GLXEW_SGI_swap_control)
	{
		std::cout << "GLXEW_SGI_swap_control" << std::endl;
		glXSwapIntervalSGI(_vSync ? 1 : 0);
	}

	glGetIntegerv(GL_MAJOR_VERSION, &_glMajorVersion);
	glGetIntegerv(GL_MINOR_VERSION, &_glMinorVersion);

	_inited = true;

	std::cout << "Running on OpenGL " << glGetString(GL_VERSION)
	          << " with GLX " << glxMajorVersion << "." << glxMinorVersion
	          << std::endl;
}

void X11Context::swapBuffers()
{
	glXSwapBuffers(_display, _glxWindow);
}

void X11Context::setWindowTitle(const String& title)
{
	_windowTitle = title;
	if (!_initiating)
		return;

	XTextProperty textProp;
	String localTitle = title;
	char* cStrTitle = strdup(localTitle.toCString());
	Xutf8TextListToTextProperty(_display, &cStrTitle, 1,
	                            XUTF8StringStyle, &textProp);
	XSetTextProperty(_display, _window, &textProp, _atomWinName);
	free(cStrTitle);
	XFree(textProp.value);
}

void X11Context::setWindowSize(int width, int height)
{
	_windowWidth = width;
	_windowHeight = height;
	if (_window == 0)
		return;

	XResizeWindow(_display, _window, _windowWidth, _windowHeight);
}

void X11Context::setWindowMaximized(bool maximized)
{
	_isMaximized = maximized;
	if (!_initiating)
		return;

	XEvent event;
	std::memset(&event, 0, sizeof event);
	event.xany.type = ClientMessage;
	event.xclient.message_type = _atomState;
	event.xclient.format = 32;
	event.xclient.window = _window;
	event.xclient.data.l[0] = _isMaximized ? 1 : 0;
	event.xclient.data.l[1] = _atomStateMaxiVert;
	event.xclient.data.l[2] = _atomStateMaxiHoriz;
	event.xclient.data.l[3] = 0;
	XSendEvent(_display, DefaultRootWindow(_display), 0,
	           SubstructureNotifyMask | SubstructureRedirectMask,
	           &event);
}

void X11Context::setWindowPosition(int x, int y)
{
	_windowX = x;
	_windowY = y;
	_movedWindow = true;
	if (!_initiating)
		return;

	XMoveWindow(_display, _window, _windowX, _windowY);
}

void X11Context::setFullScreen(bool fullScreen)
{
	_fullScreen = fullScreen;
	if (!_initiating)
		return;

	XEvent event;
	std::memset(&event, 0, sizeof event);
	event.xany.type = ClientMessage;
	event.xclient.message_type = _atomState;
	event.xclient.format = 32;
	event.xclient.window = _window;
	event.xclient.data.l[0] = _fullScreen ? 1 : 0;
	event.xclient.data.l[1] = _atomStateFullscreen;
	event.xclient.data.l[3] = 0;
	XSendEvent(_display, DefaultRootWindow(_display), 0,
	           SubstructureNotifyMask | SubstructureRedirectMask,
	           &event);
}

void X11Context::setWindowBorderLess(bool borderLess)
{
	_borderLess = borderLess;
	if (!_initiating)
		return;

	Atom atomWmHints = XInternAtom(_display, "_MOTIF_WM_HINTS", True);
	if (atomWmHints == None)
		return;

	struct
	{
		unsigned long flags;
		unsigned long functions;
		unsigned long decorations;
		long input_mode;
		unsigned long status;
	} hints = {1 << 1, 0, (borderLess ? 0ul : 1ul), 0, 0};
	XChangeProperty(_display, _window, atomWmHints, atomWmHints, 32,
	                PropModeReplace, reinterpret_cast<unsigned char*>(&hints),
	                (sizeof hints) / 4);
}

void X11Context::setCursorVisibility(bool visible)
{
	_cursorVisible = visible;
	if (!_initiating)
		return;

	if (visible)
	{
		Cursor cursor = XCreateFontCursor(_display, 68);
		XDefineCursor(_display, _window, cursor);
		XFreeCursor(_display, cursor);
	}
	else
	{
		Cursor empty;
		char data[1] = {0};
		XColor color;
		Pixmap pixmap;

		color.red = 0;
		color.green = 0;
		color.blue = 0;
		pixmap = XCreateBitmapFromData(_display, DefaultRootWindow(_display),
		                               data, 1, 1);
		empty = XCreatePixmapCursor(_display, pixmap, pixmap, &color, &color, 0, 0);
		XFreePixmap(_display, pixmap);
		XDefineCursor(_display, _window, empty);
		XFreeCursor(_display, empty);
	}
}

void X11Context::setCursorWarped(bool warped)
{
	_cursorWarped = warped;
	//setCursorVisibility(!warped);
	if (!_initiating)
		return;


	//XGrabPointer(_display, _window, False, PointerMotionMask, GrabModeAsync, GrabModeAsync, None, None, CurrentTime);
}

//----------------------------------------------------------------------------//

bool X11Context::hasPendingEvents()
{
	XEvent xev;

	while (XPending(_display) > 0)
	{
		XNextEvent(_display, &xev);
		if (xev.type == KeyPress)
		{
			char str[8];
			Xutf8LookupString(_ic, &(xev.xkey), str, sizeof str, nullptr, nullptr);
			_eventsQueue.push(new KeyPressEvent(_keyCodes.get(xev.xkey.keycode,
			                                                  KeyEvent::Unknown),
			                                    String(str)));
		}
		else if (xev.type == KeyRelease)
		{
			if (!_keyRepeat && XEventsQueued(_display, QueuedAfterReading) > 0)
			{
				XEvent nextEv;
				XPeekEvent(_display, &nextEv);
				if (nextEv.type == KeyPress && nextEv.xkey.time == xev.xkey.time
						&& nextEv.xkey.keycode == xev.xkey.keycode)
				{
					XNextEvent(_display, &nextEv);
					continue;
				}
			}

			char str[8];
			Xutf8LookupString(_ic, &(xev.xkey), str, sizeof str, nullptr, nullptr);
			_eventsQueue.push(new KeyReleaseEvent(_keyCodes.get(xev.xkey.keycode,
			                                                    KeyEvent::Unknown),
			                                      String(str)));
		}
		else if (xev.type == ClientMessage
						 && static_cast<unsigned long>(xev.xclient.data.l[0])
									== _atomWinClose)
			_eventsQueue.push(new WindowClosedEvent);
		else if (xev.type == ConfigureNotify)
		{
			if (xev.xconfigure.width != _windowWidth
					|| xev.xconfigure.height != _windowHeight)
			{
				_windowWidth = xev.xconfigure.width;
				_windowHeight = xev.xconfigure.height;
				_eventsQueue.push(new WindowResizedEvent(_windowWidth, _windowHeight));
			}
			// TODO: move event
		}
		else if (xev.type == ButtonPress)
		{
			_eventsQueue.push(
			  new MouseButtonPressEvent(_mouseButtons[xev.xbutton.button])
			);
		}
		else if (xev.type == ButtonRelease)
		{
			_eventsQueue.push(
			  new MouseButtonReleaseEvent(_mouseButtons[xev.xbutton.button])
			);
		}
		else if (xev.type == MotionNotify)
		{
			std::cout << "move (" << xev.xmotion.x_root << " ; " << xev.xmotion.y_root << ")" << std::endl;
		}
	}

	return !_eventsQueue.isEmpty();
}

Event* X11Context::popEvent()
{
	if (_eventsQueue.isEmpty())
		return nullptr;
	return _eventsQueue.pop();
}

void X11Context::initKeyCodes()
{
	_keyCodes[0x26] = KeyEvent::A;         _keyCodes[0x38] = KeyEvent::B;
	_keyCodes[0x36] = KeyEvent::C;         _keyCodes[0x28] = KeyEvent::D;
	_keyCodes[0x1a] = KeyEvent::E;         _keyCodes[0x29] = KeyEvent::F;
	_keyCodes[0x2a] = KeyEvent::G;         _keyCodes[0x2b] = KeyEvent::H;
	_keyCodes[0x1f] = KeyEvent::I;         _keyCodes[0x2c] = KeyEvent::J;
	_keyCodes[0x2d] = KeyEvent::K;         _keyCodes[0x2e] = KeyEvent::L;
	_keyCodes[0x3a] = KeyEvent::M;         _keyCodes[0x39] = KeyEvent::N;
	_keyCodes[0x20] = KeyEvent::O;         _keyCodes[0x21] = KeyEvent::P;
	_keyCodes[0x18] = KeyEvent::Q;         _keyCodes[0x1b] = KeyEvent::R;
	_keyCodes[0x27] = KeyEvent::S;         _keyCodes[0x1c] = KeyEvent::T;
	_keyCodes[0x1e] = KeyEvent::U;         _keyCodes[0x37] = KeyEvent::V;
	_keyCodes[0x19] = KeyEvent::W;         _keyCodes[0x35] = KeyEvent::X;
	_keyCodes[0x1d] = KeyEvent::Y;         _keyCodes[0x34] = KeyEvent::Z;

	_keyCodes[0x13] = KeyEvent::Num0;      _keyCodes[0x0a] = KeyEvent::Num1;
	_keyCodes[0x0b] = KeyEvent::Num2;      _keyCodes[0x0c] = KeyEvent::Num3;
	_keyCodes[0x0d] = KeyEvent::Num4;      _keyCodes[0x0e] = KeyEvent::Num5;
	_keyCodes[0x0f] = KeyEvent::Num6;      _keyCodes[0x10] = KeyEvent::Num7;
	_keyCodes[0x11] = KeyEvent::Num8;      _keyCodes[0x12] = KeyEvent::Num9;
	_keyCodes[0x31] = KeyEvent::BackQuote; _keyCodes[0x14] = KeyEvent::Hyphen;
	_keyCodes[0x15] = KeyEvent::Equal;     _keyCodes[0x16] = KeyEvent::Backspace;

	_keyCodes[0x17] = KeyEvent::Tab;      _keyCodes[0x22] = KeyEvent::LeftBracket;
	_keyCodes[0x23] = KeyEvent::RightBracket;
	_keyCodes[0x33] = KeyEvent::Backslash;
	_keyCodes[0x42] = KeyEvent::CapsLock;   _keyCodes[0x2f] = KeyEvent::SemiColon;
	_keyCodes[0x30] = KeyEvent::Apostrophe; _keyCodes[0x24] = KeyEvent::Enter;
	_keyCodes[0x32] = KeyEvent::LeftShift;
	_keyCodes[0x5e] = KeyEvent::ISOExtraLeft;
	_keyCodes[0x3b] = KeyEvent::Comma;     _keyCodes[0x3c] = KeyEvent::Period;
	_keyCodes[0x3d] = KeyEvent::Slash;     _keyCodes[0x3e] = KeyEvent::RightShift;

	_keyCodes[0x25] = KeyEvent::LeftCtrl;  _keyCodes[0x85] = KeyEvent::LeftMeta;
	_keyCodes[0x40] = KeyEvent::LeftAlt;   _keyCodes[0x41] = KeyEvent::Space;
	_keyCodes[0x6c] = KeyEvent::AltGr;     _keyCodes[0x86] = KeyEvent::RightMeta;
	_keyCodes[0x87] = KeyEvent::ContextMenu;
	_keyCodes[0x69] = KeyEvent::RightCtrl;

	_keyCodes[0x09] = KeyEvent::Escape;    _keyCodes[0x43] = KeyEvent::F1;
	_keyCodes[0x44] = KeyEvent::F2;        _keyCodes[0x45] = KeyEvent::F3;
	_keyCodes[0x46] = KeyEvent::F4;        _keyCodes[0x47] = KeyEvent::F5;
	_keyCodes[0x48] = KeyEvent::F6;        _keyCodes[0x49] = KeyEvent::F7;
	_keyCodes[0x4a] = KeyEvent::F8;        _keyCodes[0x4b] = KeyEvent::F9;
	_keyCodes[0x4c] = KeyEvent::F10;       _keyCodes[0x5f] = KeyEvent::F11;
	_keyCodes[0x60] = KeyEvent::F12;

	_keyCodes[0x71] = KeyEvent::Left;      _keyCodes[0x72] = KeyEvent::Right;
	_keyCodes[0x67] = KeyEvent::Up;        _keyCodes[0x74] = KeyEvent::Down;
	_keyCodes[0x76] = KeyEvent::Insert;    _keyCodes[0x77] = KeyEvent::Delete;
	_keyCodes[0x6e] = KeyEvent::Home;      _keyCodes[0x73] = KeyEvent::End;
	_keyCodes[0x70] = KeyEvent::PageUp;    _keyCodes[0x75] = KeyEvent::PageDown;

	_keyCodes[0x5a] = KeyEvent::NumPad0;   _keyCodes[0x57] = KeyEvent::NumPad1;
	_keyCodes[0x58] = KeyEvent::NumPad2;   _keyCodes[0x59] = KeyEvent::NumPad3;
	_keyCodes[0x53] = KeyEvent::NumPad4;   _keyCodes[0x54] = KeyEvent::NumPad5;
	_keyCodes[0x55] = KeyEvent::NumPad6;   _keyCodes[0x4f] = KeyEvent::NumPad7;
	_keyCodes[0x50] = KeyEvent::NumPad8;   _keyCodes[0x51] = KeyEvent::NumPad9;
	_keyCodes[0x6a] = KeyEvent::NumPadSlash;
	_keyCodes[0x3f] = KeyEvent::NumPadAsterisk;
	_keyCodes[0x52] = KeyEvent::NumPadHyphen;
	_keyCodes[0x56] = KeyEvent::NumPadPlus;
	_keyCodes[0x68] = KeyEvent::NumPadEnter;
	_keyCodes[0x5b] = KeyEvent::NumPadPeriod;
}

void X11Context::initButtonCodes()
{
	_mouseButtons[1] = MouseButtonEvent::LeftClick;
	_mouseButtons[2] = MouseButtonEvent::WheelClick;
	_mouseButtons[3] = MouseButtonEvent::RightClick;
	_mouseButtons[4] = MouseButtonEvent::WheelUp;
	_mouseButtons[5] = MouseButtonEvent::WheelDown;
	_mouseButtons[8] = MouseButtonEvent::Previous;
	_mouseButtons[9] = MouseButtonEvent::Next;
}

} // ns GLE_NS

#endif // GLE_WINDOW_SYSTEM == GLE_X11
