/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief Contexts/Win32Context.cpp
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-09-21
 */

#include "GLE/GLEngine.h"

#if GLE_WINDOW_SYSTEM == GLE_WIN32

#include "GLE/OpenGL.h"
#include "GLE/Contexts/Win32Context.h"

#include <iostream>

namespace GLE_NS {

Win32Context::Win32Context()
  : GLContext()
{

}

Win32Context::~Win32Context()
{

}

//----------------------------------------------------------------------------//

LPCWSTR toWin32String(const char* str)
{
	int size = MultiByteToWideChar(CP_UTF8, 0, str, -1, nullptr, 0);
	WCHAR* out = new WCHAR[size];
	if (MultiByteToWideChar(CP_UTF8, 0, str, -1, out, size) == 0)
		return nullptr;
	return out;
}

String fromWin32WString(LPTSTR str)
{
	int size = WideCharToMultiByte(CP_UTF8, 0, str, -1, nullptr, 0, nullptr, 0);
	CHAR* out = new CHAR[size];
	if (WideCharToMultiByte(CP_UTF8, 0, str, -1, out, size, nullptr, 0) == 0)
		return "(no message)";
	return String(out);
}

String win32ErrorMessage(DWORD msgId)
{
	LPTSTR message;
	FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER, 0, msgId,
	              MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US),
	              reinterpret_cast<LPTSTR>(&message), 0, nullptr);
	return fromWin32WString(message);
}

String lastErrMsg()
{
	return win32ErrorMessage(GetLastError());
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

void Win32Context::fatal(const String& msg)
{
	std::cerr << "GLEngine: " << msg << std::endl;
	LPCWSTR winStr = toWin32String(msg.toCString());
	MessageBox(nullptr, winStr, TEXT("GLEngine: failed to initialize"),
	           MB_ICONERROR);
	delete[] winStr;
	exit(1);
}

void info(const String& msg)
{
	LPCWSTR winStr = toWin32String(msg.toCString());
	MessageBox(nullptr, winStr, TEXT("GLEngine: failed to initialize"),
	           MB_ICONINFORMATION);
	delete[] winStr;
}

void Win32Context::init()
{
	WNDCLASS winClass;
	winClass.style         = CS_OWNDC;
	winClass.lpfnWndProc   = WndProc;
	winClass.cbClsExtra    = 0;
	winClass.cbWndExtra    = 0;
	winClass.hInstance     = GetModuleHandle(nullptr);
	winClass.hIcon         = LoadIcon(nullptr, IDI_WINLOGO);
	winClass.hCursor       = LoadCursor(nullptr, IDC_ARROW);
	winClass.hbrBackground = nullptr;
	winClass.lpszMenuName  = nullptr;
	winClass.lpszClassName = TEXT("OGL_CLASS");

	if (RegisterClass(&winClass) == 0)
		fatal("Win32: failed to register window class: ?.");

	createWindow();

	PIXELFORMATDESCRIPTOR pixelFormat;
	pixelFormat.nSize    = sizeof (PIXELFORMATDESCRIPTOR);
	pixelFormat.nVersion = 1;
	pixelFormat.dwFlags  = PFD_DRAW_TO_WINDOW
	                       | PFD_SUPPORT_OPENGL
	                       | PFD_GENERIC_ACCELERATED;
	if (_doubleBuffering)
		pixelFormat.dwFlags |= PFD_DOUBLEBUFFER;

	pixelFormat.iPixelType      = PFD_TYPE_RGBA;
	pixelFormat.cColorBits      = 32;
	pixelFormat.cRedBits        = 0;
	pixelFormat.cRedShift       = 0;
	pixelFormat.cGreenBits      = 0;
	pixelFormat.cGreenShift     = 0;
	pixelFormat.cBlueBits       = 0;
	pixelFormat.cBlueShift      = 0;
	pixelFormat.cAlphaBits      = 0;
	pixelFormat.cAlphaShift     = 0;
	pixelFormat.cAccumBits      = 0;
	pixelFormat.cAccumRedBits   = 0;
	pixelFormat.cAccumGreenBits = 0;
	pixelFormat.cAccumBlueBits  = 0;
	pixelFormat.cAccumAlphaBits = 0;
	pixelFormat.cDepthBits      = 16;
	pixelFormat.cStencilBits    = 8;
	pixelFormat.cAuxBuffers     = 0;

	if (!SetPixelFormat(_dc, 1, &pixelFormat))
		fatal("Win32: failed to set pixel format.");
	if ((_glContext = wglCreateContext(_dc)) == nullptr)
		fatal("WGL: failed to create dummy OpenGL context.");
	if (!wglMakeCurrent(_dc, _glContext))
		fatal("WGL: failed to set dummy OpenGL context as current.");

	glewExperimental = true;
	if (glewInit() != GLEW_OK)
		fatal("GLEW: failed to initialize.");

	wglMakeCurrent(nullptr, nullptr);

	wglDeleteContext(_glContext);
	ReleaseDC(_window, _dc);
	DestroyWindow(_window);
	_window = nullptr;

	createWindow();

	int pixAttribs[] = {
	  WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
	  WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
		WGL_ACCELERATION_ARB,   WGL_FULL_ACCELERATION_ARB,
	  WGL_COLOR_BITS_ARB,     24,
	  WGL_DEPTH_BITS_ARB,     16,
	  WGL_DOUBLE_BUFFER_ARB,  (_doubleBuffering ? GL_TRUE : GL_FALSE),
	  WGL_PIXEL_TYPE_ARB,     WGL_TYPE_RGBA_ARB,
	  0
	};

	int  pixFormat;
	UINT formatsCount;
	info("14.2");
	if (wglChoosePixelFormatARB == nullptr)
		fatal("WGL: failed to retrieve function `wglChoosePixelFormatARB` from "
		      "extension `WGL_ARB_pixel_format`.");
	if (!wglChoosePixelFormatARB(_dc, pixAttribs, nullptr, 1,
	                             &pixFormat, &formatsCount))
		fatal("WGL: failed to get a valid pixel format.");
	info("15");

	if (!SetPixelFormat(_dc, pixFormat, &pixelFormat))
	{
		fatal(String("Win32: failed to set pixel format: %1.")
		      .format(lastErrMsg()));
	}
	info("16");

	GLint attribs[] = {
	  WGL_CONTEXT_MAJOR_VERSION_ARB, _glMajorVersion,
	  WGL_CONTEXT_MINOR_VERSION_ARB, _glMinorVersion,
	  WGL_CONTEXT_FLAGS_ARB,         WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB,
	  WGL_CONTEXT_PROFILE_MASK_ARB,  _compatibilityProfile
	                                   ? WGL_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB
                                     : WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
	  0, 0
	};
	if ((_glContext = wglCreateContextAttribsARB(_dc, nullptr, attribs))
	     == nullptr)
	{
		fatal(String("WGL: failed to create OpenGL context: %1.")
		      .format(lastErrMsg()));
	}
	info("17");

	wglMakeCurrent(_dc, _glContext);
	info("18");

	ShowWindow(_window, SW_SHOW);
	SetForegroundWindow(_window);
	SetFocus(_window);

	wglSwapIntervalEXT(_vSync ? 1 : 0);

	glGetIntegerv(GL_MAJOR_VERSION, &_glMajorVersion);
	glGetIntegerv(GL_MINOR_VERSION, &_glMinorVersion);

	_inited = true;

	fatal(String("Running on OpenGL ") + String((const char*)glGetString(GL_VERSION)) + " with WGL ");

	std::cout << "Running on OpenGL " << glGetString(GL_VERSION)
	          << " with WGL" << std::endl;
}

void Win32Context::createWindow()
{
	LPCWSTR title = toWin32String(_windowTitle.toCString());

	_window = CreateWindow(TEXT("OGL_CLASS"),
	                       title,
	                       WS_OVERLAPPEDWINDOW,
	                       CW_USEDEFAULT, CW_USEDEFAULT,
	                       _windowWidth, _windowHeight,
	                       nullptr, nullptr, GetModuleHandle(nullptr), nullptr);
	delete title;
	if (_window == nullptr)
		fatal("Win32: failed to create window: ?.");

	if ((_dc = GetDC(_window)) == nullptr)
		fatal("Win32: failed to retrieve device context.");
}

void Win32Context::swapBuffers()
{
	SwapBuffers(_dc);
}

} // ns GLE_NS

#endif // GLE_WINDOW_SYSTEM == GLE_WIN32
