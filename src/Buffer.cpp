/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief Buffer.cpp
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-09-22
 */

#include "GLE/Buffer.h"

#include "GLE/OpenGL.h"

namespace GLE_NS {

Buffer::Buffer(Type type)
  : GLObject(),
    _type(type),
    _dataType(Buffer::NoType),
    _count(0)
{
	glGenBuffers(1, &_id);
}

Buffer::~Buffer()
{
	glDeleteBuffers(1, &_id);
}

//----------------------------------------------------------------------------//

void Buffer::setData(const float* data, int count, Usage usage)
{
	_dataType = Buffer::Float;
	bind();
	_count = count;
	glBufferData(_type, _count * sizeof (float),
	             data, static_cast<GLenum>(usage));
}

void Buffer::setData(const List<float>& data, Usage usage)
{
	setData(data.toArray(), data.count(), usage);
}

void Buffer::bind()
{
	glBindBuffer(_type, _id);
}

//----------------------------------------------------------------------------//

void Buffer::setType(Type type)
{ _type = type; }

Buffer::Type Buffer::type() const
{ return _type; }

Buffer::DataType Buffer::dataType() const
{ return _dataType; }

int Buffer::count() const
{ return _count; }

} // ns GLE_NS
