/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief Application.cpp
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-09-28
 */

#include "GLE/Application.h"

#include "GLE/Contexts/GLContext.h"

#include "GLE/Events/Event.h"
#include "GLE/Events/KeyEvent.h"

#include "GLE/OpenGL.h"

namespace GLE_NS {

Application* Application::_instance = nullptr;

Application::Application()
  : _running(true),
    _returnCode(0),
    _context(GLContext::create())
{
	_instance = this;
}

Application::~Application()
{
	delete _context;
}


void Application::init()
{
	_context->init();
}

void Application::startFrame()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Application::endFrame()
{
	_context->swapBuffers();
}

int Application::run()
{
	if (!_context->isInitialized())
		_context->init();

	while (_running)
	{
		while (_context->hasPendingEvents())
		{
			Event* event = _context->popEvent();
			if (event == nullptr)
				continue;

			if (event->type() == Event::KeyPressEvent)
			{
				KeyEvent* e = dynamic_cast<KeyEvent*>(event);
				if (e->key() == KeyEvent::Escape || e->text() == "q")
					_running = false;
				delete e;
			}
			else if (event->type() == Event::KeyReleaseEvent)
			{
				KeyEvent* e = dynamic_cast<KeyEvent*>(event);
				if (e->key() == KeyEvent::Home)
					_running = false;
				delete e;
			}
			else if (event->type() == Event::WindowClosedEvent)
				_running = false;
		}

		startFrame();
		endFrame();
	}

	return _returnCode;
}

void Application::exit(int returnCode)
{
	_running = false;
	_returnCode = returnCode;
}

//----------------------------------------------------------------------------//

Application* Application::get()
{ return _instance; }

//----------------------------------------------------------------------------//

bool Application::isRunning() const
{ return _running; }

GLContext* Application::context() const
{ return _context; }

bool Application::hasPendingEvents() const
{ return _context->hasPendingEvents(); }

Event* Application::popEvent() const
{ return _context->popEvent(); }

Event* Application::waitForEvent() const
{ return _context->popEvent(); }

} // ns GLE_NS
