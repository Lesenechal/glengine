/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief Events/MouseEvent.cpp
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-10-06
 */

#include "GLE/Events/MouseEvent.h"

namespace GLE_NS {

MouseMoveEvent::MouseMoveEvent(int x, int y)
  : Event(Event::MouseMoveEvent),
    _x(x),
    _y(y)
{}

int MouseMoveEvent::x() const
{ return _x; }

int MouseMoveEvent::y() const
{ return _y; }

//----------------------------------------------------------------------------//

MouseButtonEvent::MouseButtonEvent(Event::Type type, Button button)
  : Event(type),
    _button(button)
{}

MouseButtonEvent::Button MouseButtonEvent::button() const
{ return _button; }

} // ns GLE_NS
