/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief Events/WindowEvent.cpp
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-10-05
 */

#include "GLE/Events/WindowEvent.h"

namespace GLE_NS {

WindowResizedEvent::WindowResizedEvent(int width, int height)
  : Event(Event::WindowResizedEvent),
    _width(width),
    _height(height)
{}

int WindowResizedEvent::width() const
{ return _width; }

int WindowResizedEvent::height() const
{ return _height; }

} // ns GLE_NS
