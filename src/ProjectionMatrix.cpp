/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief ProjectionMatrix.cpp
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-09-20
 */

#include "GLE/ProjectionMatrix.h"

namespace GLE_NS {

ProjectionMatrix::ProjectionMatrix()
  : StackedMatrix()
{}

void ProjectionMatrix::perspective(float angle,
                                   float ratio,
                                   float near,
                                   float far)
{
	apply(perspectiveMatrix(angle, ratio, near, far));
}

//----------------------------------------------------------------------------//

Matrix4 ProjectionMatrix::perspectiveMatrix(float angle,
                                            float ratio,
                                            float near,
                                            float far)
{
	Matrix4 mat;
	float f = 1.0 / std::tan(angle / 2.0);

	mat(0, 0) = f / ratio;
	mat(1, 1) = f;
	mat(2, 2) = (far + near) / (near - far);
	mat(2, 3) = -1;
	mat(3, 2) = (2 * far * near) / (near - far);
	return mat;
}

} // ns GLE_NS
