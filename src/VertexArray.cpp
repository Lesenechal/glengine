/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief VertexArray.cpp
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-09-22
 */

#include "GLE/VertexArray.h"
#include "GLE/Buffer.h"

#include "GLE/OpenGL.h"

namespace GLE_NS {

VertexArray::VertexArray()
  : GLObject()
{
	glGenVertexArrays(1, &_id);
}

void VertexArray::setBuffer(int     index,
                            Buffer* buffer,
                            int     componentsCount,
                            bool    normalize)
{
	bind();
	buffer->bind();
	glVertexAttribPointer(index, componentsCount, buffer->dataType(),
	                      normalize, 0, nullptr);
	glEnableVertexAttribArray(index);
	_count = buffer->count() / componentsCount;
}

void VertexArray::draw(DrawMode mode, int start, int count)
{
	bind();
	glDrawArrays(mode, start, count == -1 ? _count : count);
}

void VertexArray::bind()
{
	glBindVertexArray(_id);
}

} // ns GLE_NS
