/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief ModelMatrix.cpp
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-09-20
 */

#include "GLE/ModelMatrix.h"

namespace GLE_NS {

ModelMatrix::ModelMatrix()
  : StackedMatrix()
{}

void ModelMatrix::translate(const Vector3& vec)
{
	apply(translationMatrix(vec));
}

void ModelMatrix::scale(const Vector3& vec)
{
	apply(scaleMatrix(vec));
}

void ModelMatrix::rotate(float angle, const Vector3& axis)
{
	apply(rotationMatrix(angle, axis));
}

//----------------------------------------------------------------------------//

Matrix4 ModelMatrix::translationMatrix(const Vector3& vec)
{
	Matrix4 mat;

	for (int i = 0; i < 3; i++)
		mat(3, i) = vec[i];
	return mat;
}

Matrix4 ModelMatrix::scaleMatrix(const Vector3& vec)
{
	Matrix4 mat;

	for (int i = 0; i < 3; i++)
		mat(i, i) = vec[i];
	return mat;
}

Matrix4 ModelMatrix::rotationMatrix(float angle, const Vector3& axis)
{
	Matrix4 mat;

	mat(0, 0) = axis[0] * axis[0] * (1 - std::cos(angle))
	            + std::cos(angle);
	mat(0, 1) = axis[0] * axis[1] * (1 - std::cos(angle))
	            + axis[2] * std::sin(angle);
	mat(0, 2) = axis[0] * axis[2] * (1 - std::cos(angle))
	            - axis[1] * std::sin(angle);

	mat(1, 0) = axis[0] * axis[1] * (1 - std::cos(angle))
	            - axis[2] * std::sin(angle);
	mat(1, 1) = axis[1] * axis[1] * (1 - std::cos(angle))
	            + std::cos(angle);
	mat(1, 2) = axis[1] * axis[2] * (1 - std::cos(angle))
	            + axis[0] * std::sin(angle);

	mat(2, 0) = axis[0] * axis[2] * (1 - std::cos(angle))
	            + axis[1] * std::sin(angle);
	mat(2, 1) = axis[1] * axis[2] * (1 - std::cos(angle))
	            - axis[0] * std::sin(angle);
	mat(2, 2) = axis[2] * axis[2] * (1 - std::cos(angle))
	            + std::cos(angle);

	return mat;
}

} // ns GLE_NS
