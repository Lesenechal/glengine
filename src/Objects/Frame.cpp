/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief Objects/Frame.cpp
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-09-22
 */

#include "GLE/Objects/Frame.h"

#include "GLE/ModelMatrix.h"

namespace GLE_NS {

Frame::Frame()
  : _isUpToDate(true),
    _pitch(0.0),
    _yaw(0.0),
    _roll(0.0)
{}

void Frame::updateMatrix()
{
	_matrix.setIdentity();

	float cosY = std::cos(_yaw), cosP = std::cos(_pitch), cosR = std::cos(_roll);
	float sinY = std::sin(_yaw), sinP = std::sin(_pitch), sinR = std::sin(_roll);

	_matrix(0, 0) = (cosY * cosR) + (sinY * sinP * sinR);
	_matrix(0, 1) = cosP * sinR;
	_matrix(0, 2) = (sinR * cosY * sinP) - (sinY * cosR);

	_matrix(1, 0) = (cosR * sinY * sinP) - (sinR * cosY);
	_matrix(1, 1) = cosR * cosP;
	_matrix(1, 2) = (sinY * sinR) + (cosR * cosY * sinP);

	_matrix(2, 0) = cosP * sinY;
	_matrix(2, 1) = -sinP;
	_matrix(2, 2) = cosP * cosY;

	_matrix(3, 0) = _position[0];
	_matrix(3, 1) = _position[1];
	_matrix(3, 2) = _position[2];

	_isUpToDate = true;
}

void Frame::move(const Vector3& vec)
{
	_position += vec;
	_isUpToDate = false;
}

void Frame::pitch(float angle)
{
	_pitch += angle;
	_isUpToDate = false;
}

void Frame::yaw(float angle)
{
	_yaw += angle;
	_isUpToDate = false;
}

void Frame::roll(float angle)
{
	_roll += angle;
	_isUpToDate = false;
}

void Frame::setPosition(const Vector3& position)
{
	_position = position;
	_isUpToDate = false;
}

void Frame::setPitch(float pitch)
{
	_pitch = pitch;
	_isUpToDate = false;
}

void Frame::setYaw(float yaw)
{
	_yaw = yaw;
	_isUpToDate = false;
}

void Frame::setRoll(float roll)
{
	_roll = roll;
	_isUpToDate = false;
}

//----------------------------------------------------------------------------//

Matrix4 Frame::matrix()
{
	if (!_isUpToDate)
		updateMatrix();
	return _matrix;
}

Vector3 Frame::position() const
{ return _position; }

float Frame::pitch() const
{ return _pitch; }

float Frame::yaw() const
{ return _yaw; }

float Frame::roll() const
{ return _roll; }

} // ns GLE_NS
