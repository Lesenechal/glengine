/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief Objects/Camera.cpp
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-09-20
 */

#include "GLE/Objects/Camera.h"

#include "GLE/ModelMatrix.h"

namespace GLE_NS {

Camera::Camera()
  : Frame()
{}

void Camera::updateMatrix()
{
	_matrix.setIdentity();
	_matrix.apply(ModelMatrix::rotationMatrix(-_pitch, {1.0, 0.0, 0.0}));
	_matrix.apply(ModelMatrix::rotationMatrix(-_yaw,   {0.0, 1.0, 0.0}));
	_matrix.apply(ModelMatrix::rotationMatrix(-_roll,  {0.0, 0.0, 1.0}));
	_matrix.apply(ModelMatrix::translationMatrix(-_position));
}

} // ns GLE_NS
