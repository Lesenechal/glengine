/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief Objects/Primitives/GridMesh.cpp
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-09-23
 */

#include "GLE/Objects/Primitives/GridMesh.h"

namespace GLE_NS {
namespace Meshes {

Grid::Grid(int xWidth, int zWidth, float stride)
{
	int size = ((((xWidth * 2) + 1) * 2) + (((zWidth * 2) + 1) * 2)) * 3;
	float* vertex = new float[size];
	int i = 0;

	for (int x = -xWidth; x <= xWidth; x++)
	{
		vertex[i++] = x * stride;
		vertex[i++] = 0.0;
		vertex[i++] = -zWidth;
		vertex[i++] = x * stride;
		vertex[i++] = 0.0;
		vertex[i++] = zWidth;
	}
	for (int z = -zWidth; z <= zWidth; z++)
	{
		vertex[i++] = -xWidth;
		vertex[i++] = 0.0;
		vertex[i++] = z * stride;
		vertex[i++] = xWidth;
		vertex[i++] = 0.0;
		vertex[i++] = z * stride;
	}

	_vertex.setData(vertex, size, Buffer::StaticDraw);
	_vao.setBuffer(0, &_vertex, 3);

	delete vertex;
}

void Grid::draw()
{
	_vao.draw(VertexArray::Lines);
}

} // ns Meshes
} // ns GLE_NS
