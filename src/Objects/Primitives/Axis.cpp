/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief Objects/Primitives/AxisMesh.cpp
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-09-24
 */

#include "GLE/Objects/Primitives/AxisMesh.h"

namespace GLE_NS {
namespace Meshes {

Axis::Axis()
{
	float vertex[] = {
	  0.0, 0.0, 0.0,     1.0, 0.0, 0.0,
	  0.0, 0.0, 0.0,     0.0, 1.0, 0.0,
	  0.0, 0.0, 0.0,     0.0, 0.0, 1.0
	};
	float colors[] = {
	  1.0, 0.0, 0.0,     1.0, 0.0, 0.0,
	  0.0, 1.0, 0.0,     0.0, 1.0, 0.0,
	  0.0, 0.0, 1.0,     0.0, 0.0, 1.0
	};

	_vertex.setData(vertex, 18, Buffer::StaticDraw);
	_vao.setBuffer(0, &_vertex, 3);

	_colors.setData(colors, 18, Buffer::StaticDraw);
	_vao.setBuffer(1, &_colors, 3);
}

void Axis::draw()
{
	_vao.draw(VertexArray::Lines);
}

} // ns Meshes
} // ns GLE_NS
