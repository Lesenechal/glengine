
#include "GLE/OpenGL.h"
#include "GLE/Application.h"
#include "GLE/Events/KeyEvent.h"
#include "GLE/Events/WindowEvent.h"
#include "GLE/Events/MouseEvent.h"
#include "GLE/Contexts/GLContext.h"

#include "GLE/Shaders/Program.h"
#include "GLE/Shaders/VertexShader.h"
#include "GLE/Shaders/FragmentShader.h"

#include <GL/gl.h>

#include "GLE/ModelMatrix.h"
#include "GLE/ProjectionMatrix.h"
#include "GLE/Objects/Camera.h"
#include "GLE/Buffer.h"
#include "GLE/VertexArray.h"
#include "GLE/Texture.h"

#include "GLE/Objects/Primitives/GridMesh.h"
#include "GLE/Objects/Primitives/AxisMesh.h"
#include "GLE/Objects/Primitives/UVSphere.h"

#include <cmath>

#include <unistd.h>

int main()
{
	GLE::Application app;
	app.context()->setOpenGLVersion(3, 1);
	app.context()->setWindowTitle("Hello GLEngine!");
	app.context()->setWindowSize(1024, 768);
	app.context()->setWindowPosition(0, 0);
	app.context()->setWindowBorderLess();
	app.context()->setWindowMaximized();
	app.init();

	glClearColor(0.224, 0.365, 0.569, 1.0);

	GLE::VertexShader* vertShader = new GLE::VertexShader("myShader.vert");
	GLE::FragmentShader* fragShader = new GLE::FragmentShader("myShader.frag");

	GLE::Program* p = new GLE::Program;
	p->addShader(vertShader);
	p->addShader(fragShader);
	p->bindAttribute(0, "_pos");
	//p->bindAttribute(1, "_inColor");
	p->bindAttribute(2, "_inTexCoord");
	p->bindAttribute(3, "_inNormal");
	p->link();
	p->use();

	p->setUniform("_tex", 0);
	p->setUniform("_nmap", 1);

	//Meshes::Grid grid;
	//Meshes::Axis axis;

	float flat[] = {
		-0.5,  0.5,  0.5,
		-0.5, -0.5,  0.5,
		 0.5,  0.5,  0.5,
		 0.5, -0.5,  0.5,

		 0.5,  0.5,  0.5,
		 0.5, -0.5,  0.5,
		 0.5,  0.5, -0.5,
		 0.5, -0.5, -0.5,

		 0.5,  0.5, -0.5,
		 0.5, -0.5, -0.5,
		-0.5,  0.5, -0.5,
		-0.5, -0.5, -0.5,

		-0.5,  0.5, -0.5,
		-0.5, -0.5, -0.5,
		-0.5,  0.5,  0.5,
		-0.5, -0.5,  0.5
	};
	float normals[] = {
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,

		1.0, 0.0, 0.0,
		1.0, 0.0, 0.0,
		1.0, 0.0, 0.0,
		1.0, 0.0, 0.0,

		0.0, 0.0, -1.0,
		0.0, 0.0, -1.0,
		0.0, 0.0, -1.0,
		0.0, 0.0, -1.0,

		-1.0, 0.0, 0.0,
		-1.0, 0.0, 0.0,
		-1.0, 0.0, 0.0,
		-1.0, 0.0, 0.0
	};
	float texCoords[] = {
		0.0, 1.0,
		0.0, 0.0,
		1.0, 1.0,
		1.0, 0.0,

		0.0, 1.0,
		0.0, 0.0,
		1.0, 1.0,
		1.0, 0.0,

		0.0, 1.0,
		0.0, 0.0,
		1.0, 1.0,
		1.0, 0.0,

		0.0, 1.0,
		0.0, 0.0,
		1.0, 1.0,
		1.0, 0.0
	};

	GLE::VertexArray vao;
	GLE::ArrayBuffer posBuff, normBuff, texBuff;

	posBuff.setData(flat, 4 * 4 * 3, GLE::Buffer::StaticDraw);
	vao.setBuffer(0, &posBuff, 3);

	normBuff.setData(normals, 4 * 4 * 3, GLE::Buffer::StaticDraw);
	vao.setBuffer(3, &normBuff, 3);

	texBuff.setData(texCoords, 4 * 4 * 2, GLE::Buffer::StaticDraw);
	vao.setBuffer(2, &texBuff, 2);

	GLE::Texture tex("tex.webp");
	GLE::Texture nmap("tex.nmap.webp");

	tex.bind(0);
	nmap.bind(1);

	//Meshes::UVSphere sphere(1.0, 10, 10);

	GLE::Camera cam;
	//cam.setPosition({0.0, 0.5, 0.0});
	//cam.pitch(DEGRAD(-35));

	GLE::ModelMatrix modelMx;

	GLE::ProjectionMatrix projection;
	projection.perspective(70.0, 1920.0 / 1060.0, 0.1, 50.0);

	p->setUniform("_projectionMx", projection);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_MULTISAMPLE);

	float maxAnis;
	bool hasAnis = true;
	if (GLEW_EXT_texture_filter_anisotropic)
	{
		glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxAnis);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, maxAnis);
		std::cout << "max = " << maxAnis << std::endl;
	}

	float a = 0.0;
	bool inc = true, msaa = true, wireframe = false, fs = false;

	modelMx.translate({0, 0, -0.7});

	int turn = 0;

	while (app.isRunning())
	{
		while (app.hasPendingEvents())
		{
			GLE::Event* event = app.popEvent();
			if (event == nullptr)
				continue;

			if (event->type() == GLE::Event::WindowClosedEvent)
				app.exit();
			else if (event->type() == GLE::Event::WindowResizedEvent)
			{
				GLE::WindowResizedEvent *revent = dynamic_cast<GLE::WindowResizedEvent*>(event);
				glViewport(0, 0, revent->width(), revent->height());
			}
			else if (event->type() == GLE::Event::KeyPressEvent)
			{
				GLE::KeyEvent *kevent = dynamic_cast<GLE::KeyEvent*>(event);
				if (kevent->text() == "q")
					app.exit();
				else if (turn == 0 && kevent->key() == GLE::KeyEvent::Left)
					turn = 1;
				else if (turn == 0 && kevent->key() == GLE::KeyEvent::Right)
					turn = -1;
				else if (kevent->text() == "m")
				{
					msaa = !msaa;
					if (msaa)
						glEnable(GL_MULTISAMPLE);
					else
						glDisable(GL_MULTISAMPLE);
					app.context()->setWindowTitle(msaa ? "Multisampling activé" : "Multisampling désactivé");
				}
				else if (kevent->text() == "w")
				{
					wireframe = !wireframe;
					if (wireframe)
						glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
					else
						glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
					app.context()->setWindowTitle(wireframe ? "Wireframe activé" : "Wireframe désactivé");
				}
				else if (kevent->text() == "a")
				{
					hasAnis = !hasAnis;
					if (hasAnis)
						glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, maxAnis);
					else
						glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 0);
					app.context()->setCursorVisibility(!hasAnis);
				}
				else if (kevent->text() == "f")
					app.context()->setFullScreen(fs = !fs);
			}
			else if (event->type() == GLE::Event::KeyReleaseEvent)
				turn = 0;
			else if (event->type() == GLE::Event::MouseButtonPressEvent)
			{
				GLE::MouseButtonEvent* bevent = dynamic_cast<GLE::MouseButtonEvent*>(event);
				if (bevent->button() == GLE::MouseButtonEvent::LeftClick)
					app.context()->warpCursor();
			}
			delete event;
		}

		app.startFrame();

		if (inc)
			a += 0.01;
		else
			a -= 0.01;
		if (a >= DEGRAD(30))
			inc = false;
		else if (a <= DEGRAD(-30))
			inc = true;

		//cam.setPitch(a);
		//cam.yaw(0.02);

		modelMx.rotate(0.03 * turn, {0, 1, 0});
		modelMx.push();
		//modelMx.scale({1, 1, 1});
		//modelMx.translate({0, 0, -0.3});

		p->setUniform("_viewMx", cam.matrix());
		p->setUniform("_modelMx", modelMx);

		//grid.draw();
		//axis.draw();
		vao.draw(GLE::VertexArray::TriangleStrip, 0, 4);
		vao.draw(GLE::VertexArray::TriangleStrip, 4, 4);
		vao.draw(GLE::VertexArray::TriangleStrip, 8, 4);
		vao.draw(GLE::VertexArray::TriangleStrip, 12, 4);

		modelMx.pop();

		app.endFrame();
	}

	delete p;
	return 0;
}
