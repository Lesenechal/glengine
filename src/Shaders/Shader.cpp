/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief Shaders/Shader.cpp
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2012-03-27
 */

#include "GLE/Shaders/Shader.h"

#include "GLE/Utils/List.h"
#include "GLE/Utils/File.h"

#include <iostream> // REMOVE THIS

namespace GLE_NS {

Shader::Shader(Shader::Type type, const String& filePath)
  : GLObject(),
    _type(type)
{
	GLuint id = glCreateShader(static_cast<GLenum>(_type));
	//if (id == nullptr)   TODO: implement exception
	setObjectId(id);

	if (!filePath.isNull())
	{
		loadSourceFile(filePath);
		compile();
	}
}

Shader::~Shader()
{
	glDeleteShader(_id);
}


void Shader::loadSource(const String& source)
{
	_source = source;
	const GLchar* cSourceStr = static_cast<const GLchar*>(source.toCString());
	glShaderSource(_id, 1, &cSourceStr, nullptr);
}

void Shader::loadSourceFile(const String& sourceFile)
{
	loadSource(File::getContent(sourceFile));
}

void Shader::compile()
{
	glCompileShader(_id);

	GLint compileSuccess = GL_TRUE;
	glGetShaderiv(_id, GL_COMPILE_STATUS, &compileSuccess);
	if (compileSuccess == GL_FALSE)
	{
		GLint errorMessageSize;
		glGetShaderiv(_id, GL_INFO_LOG_LENGTH, &errorMessageSize);
		char* buffer = new char[errorMessageSize + 1];
		glGetShaderInfoLog(_id, errorMessageSize, &errorMessageSize, buffer);
		String errorMessage(buffer);
		delete[] buffer;

		std::cerr << "Failed to compile shader:" << errorMessage << std::endl;
	}
}

} // ns GLE_NS
