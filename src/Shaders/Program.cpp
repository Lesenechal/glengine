/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief Shaders/Program.cpp
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2012-03-27
 */

#include "GLE/Shaders/Program.h"
#include "GLE/Shaders/Shader.h"

namespace GLE_NS {

::List<Program*> Program::_list;
Program*       Program::_current = nullptr;

Program::Program()
  : GLObject()
{
	_id = glCreateProgram();
	//if (id == nullptr)    TODO: implement exception
	_list.append(this);
}

Program::~Program()
{
	while (_shaders.count() > 0)
	{
		Shader* shader = _shaders[0];
		removeShader(shader);
		delete shader;
	}
	if (_current == this)
	{
		glUseProgram(0);
		_current = nullptr;
	}
	glDeleteProgram(_id);
	_list.removeOne(this);
}



void Program::addShader(Shader* shader)
{
	glAttachShader(_id, shader->objectId());
	_shaders.append(shader);
}

void Program::removeShader(Shader* shader)
{
	glDetachShader(_id, shader->objectId());
	_shaders.removeOne(shader);
}

void Program::bindAttribute(int index, const String& varName)
{
	glBindAttribLocation(_id, index, varName.toCString());
}

//----------------------------------------------------------------------------//

void Program::setUniform(const String& varName, const Matrix2& mat2)
{
	glUniformMatrix2fv(uniformLocation(varName), 1, GL_FALSE, mat2.values());
}

void Program::setUniform(const String& varName, const Matrix3& mat3)
{
	glUniformMatrix3fv(uniformLocation(varName), 1, GL_FALSE, mat3.values());
}

void Program::setUniform(const String& varName, const Matrix4& mat4)
{
	glUniformMatrix4fv(uniformLocation(varName), 1, GL_FALSE, mat4.values());
}

void Program::setUniform(const String& varName, const Matrix2x3& mat2x3)
{
	glUniformMatrix2x3fv(uniformLocation(varName), 1, GL_FALSE, mat2x3.values());
}

void Program::setUniform(const String& varName, const Matrix3x2& mat3x2)
{
	glUniformMatrix3x2fv(uniformLocation(varName), 1, GL_FALSE, mat3x2.values());
}

void Program::setUniform(const String& varName, const Matrix2x4& mat2x4)
{
	glUniformMatrix2x4fv(uniformLocation(varName), 1, GL_FALSE, mat2x4.values());
}

void Program::setUniform(const String& varName, const Matrix4x2& mat4x2)
{
	glUniformMatrix4x2fv(uniformLocation(varName), 1, GL_FALSE, mat4x2.values());
}

void Program::setUniform(const String& varName, const Matrix3x4& mat3x4)
{
	glUniformMatrix3x4fv(uniformLocation(varName), 1, GL_FALSE, mat3x4.values());
}

void Program::setUniform(const String& varName, const Matrix4x3& mat4x3)
{
	glUniformMatrix4x3fv(uniformLocation(varName), 1, GL_FALSE, mat4x3.values());
}

void Program::setUniform(const String& varName, int n)
{
	glUniform1i(uniformLocation(varName), n);
}

//----------------------------------------------------------------------------//

void Program::link()
{
	glLinkProgram(_id);
}

void Program::use()
{
	glUseProgram(_id);
	_current = this;
}

//----------------------------------------------------------------------------//

::List<Shader*> Program::shaders() const
{ return _shaders; }

GLuint Program::uniformLocation(const String& name) const
{
	return glGetUniformLocation(_id, name.toCString());
}

//----------------------------------------------------------------------------//

Program* Program::byId(GLuint id)
{
	for (int i = 0, c = _list.count(); i < c; i++)
	{
		if (_list[i]->objectId() == id)
			return _list[i];
	}
	return nullptr;
}

::List<Program*> Program::list()
{ return _list; }

} // ns GLE_NS
