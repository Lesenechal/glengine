TEMPLATE = lib
CONFIG += debug

CONFIG -= console
CONFIG -= app_bundle
CONFIG -= qt

DESTDIR = ../lib
TARGET  = glengine

INCLUDEPATH += include

QMAKE_CXXFLAGS         = -std=c++11
QMAKE_CXXFLAGS_WARN_ON = -pedantic -Wall -Wextra
QMAKE_CXXFLAGS_DEBUG   = -Og -g
QMAKE_CXXFLAGS_RELEASE = -Ofast

unix {
	LIBS += -lX11 -lGLEW -lGL -lturbojpeg -lpng -lwebpdecoder
}
win32 {
	LIBS += -lglew32s -lopengl32
	QMAKE_CXXFLAGS += -DGLEW_STATIC
	QMAKE_LFLAGS += -mwindows
}

SOURCES += \
    src/Contexts/GLContext.cpp \
    src/Contexts/FreeGlutContext.cpp \
    src/Shaders/VertexShader.cpp \
    src/Shaders/Shader.cpp \
    src/Shaders/Program.cpp \
    src/Utils/StringList.cpp \
    src/Utils/String.cpp \
    src/Utils/IODevice.cpp \
    src/Utils/File.cpp \
    src/Utils/ByteArray.cpp \
    src/Shaders/FragmentShader.cpp \
    src/ProjectionMatrix.cpp \
    src/ModelMatrix.cpp \
    src/Contexts/X11Context.cpp \
    src/Contexts/Win32Context.cpp \
    src/Objects/Camera.cpp \
    src/Objects/Frame.cpp \
    src/Buffer.cpp \
    src/GLObject.cpp \
    src/VertexArray.cpp \
    src/Objects/Primitives/GridMesh.cpp \
    src/Objects/Primitives/Axis.cpp \
    src/Texture.cpp \
    src/Application.cpp \
    src/Events/Event.cpp \
    src/Events/KeyEvent.cpp \
    src/Objects/Primitives/UVSphere.cpp \
    src/Events/WindowEvent.cpp \
    src/Events/MouseEvent.cpp \
    include/GLE/StackedMatrix.inl.cpp \
    include/GLE/Utils/MultiMap.inl.cpp \
    include/GLE/Utils/OrderedMap.inl.cpp \
    include/GLE/Utils/Matrix.inl.cpp \
    include/GLE/Utils/Map.inl.cpp \
    include/GLE/Utils/List.inl.cpp \
    include/GLE/Utils/Stack.inl.cpp \
    include/GLE/Utils/Pair.inl.cpp \
    include/GLE/Utils/Vector.inl.cpp

equals(TEMPLATE, app) {
	SOURCES += src/User/Main.cpp
}

HEADERS += \
    include/GLE/OpenGL.h \
    include/GLE/GLEngine.h \
    include/GLE/Contexts/GLContext.h \
    include/GLE/Contexts/FreeGlutContext.h \
    include/GLE/Shaders/VertexShader.h \
    include/GLE/Shaders/Shader.h \
    include/GLE/Shaders/Program.h \
    include/GLE/Utils/Vector.h \
    include/GLE/Utils/StringList.h \
    include/GLE/Utils/String.h \
    include/GLE/Utils/Stack.h \
    include/GLE/Utils/Pair.h \
    include/GLE/Utils/OrderedMap.h \
    include/GLE/Utils/MultiMap.h \
    include/GLE/Utils/Matrix.h \
    include/GLE/Utils/Map.h \
    include/GLE/Utils/List.h \
    include/GLE/Utils/IODevice.h \
    include/GLE/Utils/File.h \
    include/GLE/Utils/ByteArray.h \
    include/GLE/Shaders/FragmentShader.h \
    include/GLE/ProjectionMatrix.h \
    include/GLE/ModelMatrix.h \
    include/GLE/StackedMatrix.h \
    include/GLE/Contexts/X11Context.h \
    include/GLE/Objects/Frame.h \
    include/GLE/Objects/Camera.h \
    include/GLE/Buffer.h \
    include/GLE/GLObject.h \
    include/GLE/VertexArray.h \
    include/GLE/Objects/AbstractMesh.h \
    include/GLE/Objects/Primitives/GridMesh.h \
    include/GLE/Objects/Primitives/AxisMesh.h \
    include/GLE/Texture.h \
    include/GLE/Application.h \
    include/GLE/Events/Event.h \
    include/GLE/Events/KeyEvent.h \
    include/GLE/Objects/Primitives/UVSphere.h \
    include/GLE/Events/WindowEvent.h \
    include/GLE/Events/MouseEvent.h \
    include/GLE/Contexts/Win32Context.h

SOURCES -= \
    include/GLE/Utils/Vector.inl.cpp \
    include/GLE/Utils/Stack.inl.cpp \
    include/GLE/Utils/Pair.inl.cpp \
    include/GLE/Utils/OrderedMap.inl.cpp \
    include/GLE/Utils/MultiMap.inl.cpp \
    include/GLE/Utils/Matrix.inl.cpp \
    include/GLE/StackedMatrix.inl.cpp \
    include/GLE/Utils/Map.inl.cpp \
    include/GLE/Utils/List.inl.cpp
