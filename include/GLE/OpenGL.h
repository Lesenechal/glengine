/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief OpenGL.h
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2012-03-27
 */

#ifndef _GLE_OPENGL_H_
#define _GLE_OPENGL_H_

#include "GLE/GL/glew.h"
#if defined(_WIN32)
#  include "GLE/GL/wglew.h"
#else
#  include "GLE/GL/glxew.h"
#endif // _WIN32

#define GL3_PROTOTYPES 1
#include "GLE/GL/gl3.h"

#endif // !_GLE_OPENGL_H_
