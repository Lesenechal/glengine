/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief Shaders/Program.h
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2012-03-27
 */

#ifndef _GLE_SHADERS_PROGRAM_H_
#define _GLE_SHADERS_PROGRAM_H_

#include "GLE/GLObject.h"

#include "GLE/Utils/List.h"
#include "GLE/Utils/Matrix.h"

namespace GLE_NS {

class Shader;

class Program : public GLObject
{
public:
	Program();
	~Program();

	void addShader(Shader* shader);
	void removeShader(Shader* shader);

	void bindAttribute(int index, const String& varName);

	void setUniform(const String& varName, const Matrix2& mat2);
	void setUniform(const String& varName, const Matrix3& mat3);
	void setUniform(const String& varName, const Matrix4& mat4);
	void setUniform(const String& varName, const Matrix2x3& mat2x3);
	void setUniform(const String& varName, const Matrix3x2& mat3x2);
	void setUniform(const String& varName, const Matrix2x4& mat2x4);
	void setUniform(const String& varName, const Matrix4x2& mat4x2);
	void setUniform(const String& varName, const Matrix3x4& mat3x4);
	void setUniform(const String& varName, const Matrix4x3& mat4x3);

	void setUniform(const String& varName, int n);

	void link();
	void use();

	List<Shader*> shaders() const;
	GLuint uniformLocation(const String& name) const;

public:
	static Program*       byId(GLuint id);
	static List<Program*> list();

private:
	List<Shader*> _shaders;

private:
	static List<Program*> _list;
	static Program*       _current;
};

} // ns GLE_NS

#endif // !_GLE_SHADERS_PROGRAM_H_
