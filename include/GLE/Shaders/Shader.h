/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief Shaders/Shader.h
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2012-03-27
 */

#ifndef _GLE_SHADERS_SHADER_H_
#define _GLE_SHADERS_SHADER_H_

#include "GLE/GLObject.h"
#include "GLE/OpenGL.h"

#include "GLE/Utils/String.h"

namespace GLE_NS {

template<typename T> class List;

class Shader : public GLObject
{
public:
	enum Type {
		Vertex         = GL_VERTEX_SHADER,
		TessControl    = GL_TESS_CONTROL_SHADER,
		TessEvaluation = GL_TESS_EVALUATION_SHADER,
		Geometry       = GL_GEOMETRY_SHADER,
		Fragment       = GL_FRAGMENT_SHADER
	};

	explicit Shader(Type type, const String& filePath = String());
	virtual ~Shader();

	void loadSource(const String& source);
	void loadSourceFile(const String& sourceFile);
	void compile();

private:
	Type   _type;
	String _source;
};

} // ns GLE_NS

#endif // !_GLE_SHADERS_SHADER_H_
