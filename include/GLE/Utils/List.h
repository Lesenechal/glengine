/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @file Utils/List.h
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-03-28
 */

#ifndef _GLE_UTILS_LIST_H_
#define _GLE_UTILS_LIST_H_

#include "GLE/GLEngine.h"

#include <vector>

class String;

/**
 * @class List Utils/List.h <List>
 * @brief A list (std::vector backend)
 * @param T The type of objects contained in the list
 */
template<typename T>
class List : private std::vector<T>
{
public:
	/**
	 * @brief Remove all the elements
	 */
	using std::vector<T>::clear;
	using std::vector<T>::begin;
	using std::vector<T>::end;

	/**
	 * @brief Constructs an empty list
	 */
	List();

	List(T* array, std::size_t size);

	/**
	 * @brief Constructs a list with an initializer list
	 * @param values The initializer list
	 */
	List(const std::initializer_list<T>& values);


	/**
	 * @brief Append a value to the end of the list
	 * @param value The value to append
	 */
	void    append(const T& value);

	/**
	 * @brief Append a list of values to the end of the list
	 * @param value The list of values to append
	 */
	void    append(const List<T>& values);

	/**
	 * @brief Removes the value at index @p index
	 * @param index The value's index to remove
	 */
	void    remove(int index);

	/**
	 * @brief Removes the first value equal to @p value
	 * @param value The value to remove
	 */
	void    removeOne(const T& value);

	/**
	 * @brief Removes all values equal to @p value
	 * @param value The value to remove
	 */
	void    removeAll(const T& value);

	/**
	 * @brief Tests if the list contains @p value
	 * @param value The value to test
	 * @return true if the list contains @p value, otherwise false
	 */
	bool    contains(const T& value) const;

	/**
	 * @brief Get the number of elements in the list
	 * @return The number of elements in the list
	 */
	int     count() const;

	/**
	 * @brief Reverts the list
	 * @return The reverted list
	 */
	List<T> revert() const;

	/**
	 * @brief Implements how to dump the data for debugging
	 * @return The dumped data string
	 */
	String  dump() const;

	T*       toArray();
	const T* toArray() const;

	/**
	 * @brief Access an element by index
	 * @param index The element index
	 * @return The element to the given index
	 */
	T       at(int index) const;

	/**
	 * @brief Access an element by index
	 * @param index The element index
	 * @return The element to the given index
	 */
	T       operator[](int index) const;
	T&      operator[](int index);
};

#include "List.inl.cpp"

#endif // !_GLE_UTILS_LIST_H_
