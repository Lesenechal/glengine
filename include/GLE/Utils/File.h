/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @file Utils/File.h
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-04-01
 */

#ifndef _GLE_UTILS_FILE_H_
#define	_GLE_UTILS_FILE_H_

#include "GLE/Utils/IODevice.h"
#include "GLE/Utils/String.h"

#include <fstream>

/**
 * @class File Utils/File.h <File>
 * @brief A file
 */
class File : public IODevice, private std::fstream
{
public:
	File(const String& path);

	/**
	 * @brief Implements how to open the device
	 * @brief mode The open mode
     */
	virtual void open(int mode);

	/**
	 * @brief Implements how to write datas to the device
	 */
	virtual void write(const ByteArray& datas);

	/**
	 * @brief Implements how to close the device
     */
	virtual void close();

public:
	/**
	 * @brief Get all the content of a file
	 * @param path The file path
	 * @return All the content of the file to @p path
	 */
	static ByteArray getContent(const String& path);

private:
	/**
	 * @brief The file path
	 */
	String _path;
};

#endif // !_GLE_UTILS_FILE_H_
