/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief Utils/Vector.h
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2012-03-27
 */

#ifndef _GLE_UTILS_VECTOR_H_
#define _GLE_UTILS_VECTOR_H_

#include "GLE/GLEngine.h"

#include <initializer_list>

template<typename T, int S>
class Vector
{
public:
	Vector();
	Vector(const T* values);
	Vector(std::initializer_list<T> values);

	void         dump() const;

	Vector<T, S> operator+(const Vector<T, S>& vec) const;
	Vector<T, S> operator+=(const Vector<T, S>& vec);

	Vector<T, S> operator-() const;

	Vector<T, S> operator*(T scalar) const;
	Vector<T, S> operator*=(T scalar);

	Vector<T, S> operator*(const Vector<T, S>& vec) const;
	Vector<T, S> operator*=(const Vector<T, S>& vec);

	//Vector<T, S> operator*();

	T& operator[](int index);
	T  operator[](int index) const;

	T            scalarProduct(const Vector<T, S>& vec) const;

private:
	T _values[S];
};

typedef Vector<float, 2> Vector2;
typedef Vector<float, 3> Vector3;
typedef Vector<float, 4> Vector4;

#include "Vector.inl.cpp"

#endif // !_GLE_UTILS_VECTOR_H_
