/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @file Utils/StringList.h
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-03-28
 */

#ifndef _GLE_UTILS_STRINGLIST_H_
#define _GLE_UTILS_STRINGLIST_H_

#include "GLE/GLEngine.h"

#include "GLE/Utils/List.h"
#include "GLE/Utils/String.h"

/**
 * @class StringList Utils/StringList.h <StringList>
 * @brief A list of strings
 */
class StringList : public List<String>
{
public:
	/**
	 * @brief Constructs an empty string list
	 */
	StringList();

	StringList(const List<String>& list);

	/**
	 * @brief Constructs a string list with an initializer list
	 * @param values The initializer list
	 */
	StringList(const std::initializer_list<String>& values);

	/**
	 * @brief Joins the string list with the specified delimiter
	 * @param delem The delimiter
	 * @return The joined string
	 */
	String join(const String& delem) const;

	/**
	 * @brief Dumps the data for debugging
	 * @return The dumped data string
	 */
	String dump() const;
};

#endif // !_GLE_UTILS_STRINGLIST_H_
