/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @file Utils/Stack.inl.cpp
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-03-28
 */

template<typename T>
Stack<T>::Stack()
{ }

template<typename T>
T& Stack<T>::top()
{ return std::stack<T>::top(); }

template<typename T>
T Stack<T>::top() const
{ return std::stack<T>::top(); }

template<typename T>
void Stack<T>::push(const T& value)
{ std::stack<T>::push(value); }

template<typename T>
T Stack<T>::pop()
{
	T& val = top();
	std::stack<T>::pop();
	return val;
}

template<typename T>
int Stack<T>::count() const
{ return std::stack<T>::size(); }

template<typename T>
bool Stack<T>::isEmpty() const
{ return std::stack<T>::empty(); }
