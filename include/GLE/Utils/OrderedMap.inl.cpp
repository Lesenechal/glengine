/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @file Utils/OrderedMap.inl.cpp
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-03-28
 */

template<typename Key, typename Value, typename Comp>
OrderedMap<Key, Value, Comp>::OrderedMap()
  : std::map<Key, Value, Comp>()
{ }

template<typename Key, typename Value, typename Comp>
Value OrderedMap<Key, Value, Comp>::get(const Key& key) const
{ return find(key)->second; }

template<typename Key, typename Value, typename Comp>
void OrderedMap<Key, Value, Comp>::set(const Key& key, const Value& value)
{ std::map<Key, Value, Comp>::operator[](key) = value; }

template<typename Key, typename Value, typename Comp>
void OrderedMap<Key, Value, Comp>::clear()
{ std::map<Key, Value, Comp>::clear(); }

template<typename Key, typename Value, typename Comp>
typename OrderedMap<Key, Value, Comp>::Iterator
  OrderedMap<Key, Value, Comp>::it() const
{ return OrderedMap<Key, Value, Comp>::Iterator(*this); }

template<typename Key, typename Value, typename Comp>
bool OrderedMap<Key, Value, Comp>::hasKey(const Key& key) const
{ return find(key) != std::map<Key, Value, Comp>::end(); }

template<typename Key, typename Value, typename Comp>
List<Key> OrderedMap<Key, Value, Comp>::keys() const
{
	List<Key> list;
	for (OrderedMap<Key, Value, Comp>::Iterator i = it(); i.hasNext(); i++)
		list.append(i.key());
	return list;
}

template<typename Key, typename Value, typename Comp>
List<Value> OrderedMap<Key, Value, Comp>::values() const
{
	List<Value> list;
	for (OrderedMap<Key, Value, Comp>::Iterator i = it(); i.hasNext(); i++)
		list.append(i.value());
	return list;
}

template<typename Key, typename Value, typename Comp>
Value& OrderedMap<Key, Value, Comp>::operator[](const Key& key)
{ return std::map<Key, Value, Comp>::operator[](key); }

template<typename Key, typename Value, typename Comp>
Value OrderedMap<Key, Value, Comp>::operator[](const Key& key) const
{ return find(key)->second; }

//----------------------------------------------------------------------------//

template<typename Key, typename Value, typename Comp>
OrderedMap<Key, Value, Comp>::Iterator::Iterator(const OrderedMap<Key,
                                                 Value, Comp>& map)
  : _map(&map)
{
	_iterator = ((std::map<Key, Value, Comp>*)_map)
	            ->std::map<Key, Value, Comp>::begin();
}

template<typename Key, typename Value, typename Comp>
void OrderedMap<Key, Value, Comp>::Iterator::next()
{
	//TODO: throw an exception if at the end
	_iterator++;
}

template<typename Key, typename Value, typename Comp>
bool OrderedMap<Key, Value, Comp>::Iterator::hasNext() const
{ return _iterator != _map->end(); }

template<typename Key, typename Value, typename Comp>
Key OrderedMap<Key, Value, Comp>::Iterator::key() const
{ return _iterator->first; }

template<typename Key, typename Value, typename Comp>
Value OrderedMap<Key, Value, Comp>::Iterator::value() const
{ return _iterator->second; }

template<typename Key, typename Value, typename Comp>
typename OrderedMap<Key, Value, Comp>::Iterator
  OrderedMap<Key, Value, Comp>::Iterator::operator++(int)
{
	next();
	return *this;
}
