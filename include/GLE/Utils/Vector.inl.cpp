/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief Utils/Vector.inl.cpp
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2012-03-27
 */

#include <cstring>

#include <iostream>

template<typename T, int S>
Vector<T, S>::Vector()
{
	std::memset(_values, 0, S * sizeof (T));
}

template<typename T, int S>
Vector<T, S>::Vector(const T* values)
{
	std::memcpy(_values, values, S * sizeof (T));
}

template<typename T, int S>
Vector<T, S>::Vector(std::initializer_list<T> values)
{
	int i = 0;

	for (const T& value : values)
	{
		if (i == S)
			break;
		_values[i] = value;
		i++;
	}
	for (; i < S; i++)
		_values[i] = static_cast<T>(0);
}


template<typename T, int S>
void Vector<T, S>::dump() const
{
	std::cout << "(";
	for (int i = 0; i < S; i++)
		std::cout << _values[i] << (i < S - 1 ? "; " : "");
	std::cout << ")" << std::flush;
}


//---------------------------- Vector operations -----------------------------//

template<typename T, int S>
Vector<T, S> Vector<T, S>::operator+(const Vector<T, S>& vec) const
{
	Vector<T, S> result = *this;
	for (int i = 0; i < S; i++)
		result._values[i] += vec._values[i];
	return result;
}

template<typename T, int S>
Vector<T, S> Vector<T, S>::operator+=(const Vector<T, S>& vec)
{
	return (*this = *this + vec);
}

//----------------------------------------------------------------------------//

template<typename T, int S>
Vector<T, S> Vector<T, S>::operator-() const
{
	Vector<T, S> result = *this;
	for (int i = 0; i < S; i++)
		result._values[i] = -result._values[i];
	return result;
}

//----------------------------------------------------------------------------//

template<typename T, int S>
Vector<T, S> Vector<T, S>::operator*(T scalar) const
{
	Vector<T, S> result = *this;
	for (int i = 0; i < S; i++)
		result._values[i] *= scalar;
	return result;
}

template<typename T, int S>
Vector<T, S> Vector<T, S>::operator*=(T scalar)
{
	return (*this = *this * scalar);
}

//----------------------------------------------------------------------------//

template<typename T, int S>
Vector<T, S> Vector<T, S>::operator*(const Vector<T, S>& vec) const
{
	Vector<T, S> result = *this;
	//TODO: implement this function
	return result;
}

template<typename T, int S>
Vector<T, S> Vector<T, S>::operator*=(const Vector<T, S>& vec)
{
	return (*this = *this * vec);
}

//----------------------------------------------------------------------------//

template<typename T, int S>
T& Vector<T, S>::operator[](int index)
{ return _values[index]; }

template<typename T, int S>
T Vector<T, S>::operator[](int index) const
{ return _values[index]; }

template<typename T, int S>
T Vector<T, S>::scalarProduct(const Vector<T, S>& vec) const
{
	T result = static_cast<T>(0);

	for (int i = 0; i < S; i++)
		result += _values[i] * vec._values[1];
	return result;
}
