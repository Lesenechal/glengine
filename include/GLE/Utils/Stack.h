/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @file Utils/Stack.h
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-03-28
 */

#ifndef _GLE_UTILS_STACK_H_
#define	_GLE_UTILS_STACK_H_

#include <stack>

/**
 * @class Stack Utils/Stack.h <Stack>
 * @brief A stack
 */
template<typename T>
class Stack : private std::stack<T>
{
public:
	/**
	 * @brief Constructs an empty stack
	 */
	Stack();

	T&   top();
	T    top() const;
	void push(const T& value);
	T    pop();
	int  count() const;
	bool isEmpty() const;
};

#include "Stack.inl.cpp"

#endif // !_GLE_UTILS_STACK_H_
