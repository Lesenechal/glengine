/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @file Utils/MultiMap.inl.cpp
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-03-28
 */

template<typename Key, typename Value>
MultiMap<Key, Value>::MultiMap()
  : std::multimap<Key, Value>()
{ }

template<typename Key, typename Value>
Value MultiMap<Key, Value>::getFirst(const Key& key) const
{ return find(key)->second; }

template<typename Key, typename Value>
List<Value> MultiMap<Key, Value>::get(const Key& key) const
{
	const std::pair<typename std::multimap<Key, Value>::const_iterator,
	                typename std::multimap<Key, Value>::const_iterator>
	    it = std::multimap<Key, Value>::equal_range(key);
	List<Value> list;

	for (typename std::multimap<Key, Value>::const_iterator i = it.first;
	     i != it.second; i++)
		list.append((*i).second);
	return list;
}

template<typename Key, typename Value>
void MultiMap<Key, Value>::setFirst(const Key& key, const Value& value)
{
	typename std::multimap<Key, Value>::iterator it = find(key);
	if (it == end())
		insert(key, value);
	else
		it->second = value;
}

template<typename Key, typename Value>
void MultiMap<Key, Value>::insert(const Key& key, const Value& value)
{ std::multimap<Key, Value>::insert(std::pair<Key, Value>(key, value)); }

//----------------------------------------------------------------------------//

template<typename Key, typename Value>
MultiMap<Key, Value>::Iterator::Iterator(const MultiMap<Key, Value>& map)
  : _map(&map)
{
	_iterator = ((std::multimap<Key, Value>*)_map)
	            ->std::multimap<Key, Value>::begin();
}

template<typename Key, typename Value>
void MultiMap<Key, Value>::Iterator::next()
{
	//TODO: throw an exception if at the end
	_iterator++;
}

template<typename Key, typename Value>
bool MultiMap<Key, Value>::Iterator::hasNext() const
{ return _iterator != _map->end(); }

template<typename Key, typename Value>
Key MultiMap<Key, Value>::Iterator::key() const
{ return _iterator->first; }

template<typename Key, typename Value>
Value MultiMap<Key, Value>::Iterator::value() const
{ return _iterator->second; }

template<typename Key, typename Value>
typename MultiMap<Key, Value>::Iterator
  MultiMap<Key, Value>::Iterator::operator++(int)
{
	next();
	return *this;
}
