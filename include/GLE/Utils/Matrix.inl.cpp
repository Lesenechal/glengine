/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief Utils/Matrix.inl.cpp
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-03-27
 */

#include <cstring>

#include <iostream>
#include <iomanip>

template<typename T, int W, int H>
Matrix<T, W, H>::Matrix()
{
	setIdentity();
}

//----------------------------------------------------------------------------//

template<typename T, int W, int H>
void Matrix<T, W, H>::setIdentity()
{
	std::memset(_values, 0, W * H * sizeof (T));
	for (int i = 0, s = std::min(W, H); i < s; i++)
		_values[i * H + i] = static_cast<T>(1);
}

template<typename T, int W, int H>
void Matrix<T, W, H>::apply(const Matrix<T, W, H>& mat)
{
	*this *= mat; // FIXME: wrong order (bad operator* implementation)
}

//----------------------------------------------------------------------------//

template<typename T, int W, int H>
Matrix<T, W, H> Matrix<T, W, H>::operator*(const Matrix<T, W, H>& mat) const
{
	Matrix<T, W, H> r;

	for (int x = 0; x < W; x++)
	{
		for (int y = 0; y < H; y++)
		{
			T sum = static_cast<T>(0);
			for (int i = 0; i < W; i++)
				sum += _values[i * H + y] * mat._values[x * H + i];
			r._values[x * H + y] = sum;
		}
	}
	return r;
}

template<typename T, int W, int H>
Matrix<T, W, H> Matrix<T, W, H>::operator*=(const Matrix<T, W, H>& mat)
{
	return *this = *this * mat;
}

//----------------------------------------------------------------------------//

template<typename T, int W, int H>
T* Matrix<T, W, H>::values()
{ return _values; }

template<typename T, int W, int H>
const T* Matrix<T, W, H>::values() const
{ return _values; }

//----------------------------------------------------------------------------//

template<typename T, int W, int H>
T& Matrix<T, W, H>::operator()(int x, int y)
{
	return _values[x * H + y];
}

template<typename T, int W, int H>
T Matrix<T, W, H>::operator()(int x, int y) const
{
	return _values[x * H + y];
}

template<typename T, int W, int H>
Vector<T, H> Matrix<T, W, H>::operator[](int x) const
{
	return Vector<T, H>(_values + (x * H));
}

//----------------------------------------------------------------------------//

template<typename T, int W, int H>
void Matrix<T, W, H>::dump() const
{
	for (int x = 0; x < W; x++)
	{
		if (x == 0)
			std::cout << "  ╔════════════";
		else
			std::cout << "╤════════════";
	}
	std::cout << "╗" << std::endl;

	for (int y = 0; y < H; y++)
	{
		for (int x = 0; x < W; x++)
		{
			std::cout << (x == 0 ? "  ║" : "│") << std::setw(12)
			          << _values[x * H + y];
		}
		std::cout << "║" << std::endl;

		for (int i = 0; i < W; i++)
		{
			if (y == H - 1)
				std::cout << (i == 0 ? "  ╚════════════" : "╧════════════");
			else
				std::cout << (i == 0 ? "  ╟────────────" : "┼────────────");
		}
		std::cout << (y == H - 1 ? "╝" : "╢") << std::endl;
	}
}
