 /***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief Matrix.h
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-03-27
 */

#ifndef _GLE_UTILS_MATRIX_H_
#define _GLE_UTILS_MATRIX_H_

#include "GLE/Utils/Vector.h"

template<typename T, int W, int H>
class Matrix
{
public:
	Matrix();

	void setIdentity();
	void apply(const Matrix<T, W, H>& mat);

	Matrix<T, W, H> operator*(const Matrix<T, W, H>& mat) const;
	Matrix<T, W, H> operator*=(const Matrix<T, W, H>& mat);

	T*       values();
	const T* values() const;

	T&           operator()(int x, int y);
	T            operator()(int x, int y) const;
	Vector<T, H> operator[](int x) const;

	void dump() const;

protected:
	T _values[W * H];
};

typedef Matrix<float, 2, 2> Matrix2;
typedef Matrix<float, 3, 3> Matrix3;
typedef Matrix<float, 4, 4> Matrix4;

typedef Matrix<float, 2, 3> Matrix2x3;
typedef Matrix<float, 3, 2> Matrix3x2;
typedef Matrix<float, 2, 4> Matrix2x4;
typedef Matrix<float, 4, 2> Matrix4x2;
typedef Matrix<float, 3, 4> Matrix3x4;
typedef Matrix<float, 4, 3> Matrix4x3;

#include "Matrix.inl.cpp"

#endif // !_GLE_UTILS_MATRIX_H_
