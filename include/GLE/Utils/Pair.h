/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @file Utils/Pair.h
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-03-28
 */

#ifndef _GLE_UTILS_PAIR_H_
#define	_GLE_UTILS_PAIR_H_

#include <utility>

/**
 * @class Pair Utils/Pair.h <Pair>
 * @brief A pair of values
 */
template<typename F, typename S>
class Pair : private std::pair<F, S>
{
public:
	/**
	 * @brief Constructs a pair of values
	 */
	Pair();

	Pair(const std::pair<F, S>& pair);

	/**
	 * @brief Constructs a pair of values
     * @param first The first value
     * @param second The second value
     */
	Pair(const F& first, const S& second);

	/**
	 * @brief Get the first value
	 * @return The first value
	 */
	F first() const;

	/**
	 * @brief Get the second value
	 * @return The second value
	 */
	S second() const;
};

#include "Pair.inl.cpp"

#endif // !_GLE_UTILS_PAIR_H_
