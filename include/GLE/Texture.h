/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief Texture.h
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-09-26
 */

#ifndef _GLE_TEXTURE_H_
#define _GLE_TEXTURE_H_

#include "GLE/GLObject.h"

class String;
class ByteArray;

namespace GLE_NS {

class Texture : public GLObject
{
public:
	enum Type
	{
		Texture1D                 = GL_TEXTURE_1D,
		Texture2D                 = GL_TEXTURE_2D,
		Texture3D                 = GL_TEXTURE_3D,
		Texture1DArray            = GL_TEXTURE_1D_ARRAY,
		Texture2DArray            = GL_TEXTURE_2D_ARRAY,
		Rectangle                 = GL_TEXTURE_RECTANGLE,
		CubeMap                   = GL_TEXTURE_CUBE_MAP,
		CubeMapArray              = GL_TEXTURE_CUBE_MAP_ARRAY,
		Buffer                    = GL_TEXTURE_BUFFER,
		Texture2DMultisample      = GL_TEXTURE_2D_MULTISAMPLE,
		Texture2DMultisampleArray = GL_TEXTURE_2D_MULTISAMPLE_ARRAY
	};

	Texture(const String& file, Type type = Texture2D);

	ByteArray fromJpeg(ByteArray& datas);
	ByteArray fromPng(const ByteArray& datas);
	ByteArray fromWebp(const ByteArray& datas);

	virtual void bind() override;
	void bind(int index);

private:
	Type _type;
	int  _width;
	int  _height;
};

} // ns GLE_NS

#endif // !_GLE_TEXTURE_H_
