/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief ProjectionMatrix.h
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-09-20
 */

#ifndef _GLE_PROJECTIONMATRIX_H_
#define _GLE_PROJECTIONMATRIX_H_

#include "GLE/StackedMatrix.h"

namespace GLE_NS {

class ProjectionMatrix : public StackedMatrix<float, 4, 4>
{
public:
	ProjectionMatrix();

	void perspective(float angle, float ratio, float near, float far);

public:
	static Matrix4 perspectiveMatrix(float angle,
	                                 float ratio,
	                                 float near,
	                                 float far);
};

} // ns GLE_NS

#endif // !_GLE_PROJECTIONMATRIX_H_
