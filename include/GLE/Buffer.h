/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief Buffer.h
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-09-22
 */

#ifndef _GLE_BUFFER_H_
#define _GLE_BUFFER_H_

#include "GLE/GLObject.h"
#include "GLE/Utils/List.h"

namespace GLE_NS {

class Buffer : public GLObject
{
public:
	enum Type
	{
		Array             = GL_ARRAY_BUFFER,
		AtomicCounter     = GL_ATOMIC_COUNTER_BUFFER,
		CopyRead          = GL_COPY_READ_BUFFER,
		CopyWrite         = GL_COPY_WRITE_BUFFER,
		DrawIndirect      = GL_DRAW_INDIRECT_BUFFER,
		ElementArray      = GL_ELEMENT_ARRAY_BUFFER,
		PixelPack         = GL_PIXEL_PACK_BUFFER,
		PixelUnpack       = GL_PIXEL_UNPACK_BUFFER,
		Texture           = GL_TEXTURE_BUFFER,
		TransformFeedback = GL_TRANSFORM_FEEDBACK_BUFFER,
		Uniform           = GL_UNIFORM_BUFFER
	};

	enum Usage
	{
		StreamDraw  = GL_STREAM_DRAW,
		StreamRead  = GL_STREAM_READ,
		StreamCopy  = GL_STREAM_COPY,
		StaticDraw  = GL_STATIC_DRAW,
		StaticRead  = GL_STATIC_READ,
		StaticCopy  = GL_STATIC_COPY,
		DynamicDraw = GL_DYNAMIC_DRAW,
		DynamicRead = GL_DYNAMIC_READ,
		DynamicCopy = GL_DYNAMIC_COPY
	};

	enum DataType
	{
		NoType        = 0,
		Byte          = GL_BYTE,
		UnsignedByte  = GL_UNSIGNED_BYTE,
		Short         = GL_SHORT,
		UnsignedShort = GL_UNSIGNED_SHORT,
		Int           = GL_INT,
		UnsignedInt   = GL_UNSIGNED_INT,
		Float         = GL_FLOAT,
		Double        = GL_DOUBLE
	};

	Buffer(Type type);
	virtual ~Buffer();

	void setData(const float* data, int count, Usage usage);
	void setData(const List<float>& data, Usage usage);

	virtual void bind() override;

	void setType(Type type);

	Type     type() const;
	DataType dataType() const;
	int      count() const;

private:
	Type     _type;
	DataType _dataType;
	int      _count;
};

//----------------------------------------------------------------------------//

class ArrayBuffer : public Buffer
{
public:
	ArrayBuffer()
	  : Buffer(Buffer::Array)
	{}
	virtual ~ArrayBuffer(){}
};

} // ns GLE_NS

#endif // !_GLE_BUFFER_H_
