/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief Contexts/X11Context.h
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-04-01
 */

#ifndef _GLE_CONTEXTS_X11CONTEXT_H_
#define _GLE_CONTEXTS_X11CONTEXT_H_

#include "GLE/GLEngine.h"

#if WINDOW_SYSTEM == WS_X11

#include "GLE/Contexts/GLContext.h"

#include "GLE/Events/KeyEvent.h"
#include "GLE/Events/MouseEvent.h"

#include "GLE/Utils/Stack.h"

#include <X11/Xlib.h>
#define GLX_GLXEXT_PROTOTYPES 1
#include <GL/glx.h>

namespace GLE_NS {

class X11Context : public GLContext
{
public:
	X11Context();
	virtual ~X11Context();

	virtual void init() override;
	virtual void swapBuffers() override;

	virtual void setWindowTitle(const String& title) override;
	virtual void setWindowSize(int width, int height) override;

	/**
	 * @brief Sets the window as maximized (takes all available space)
	 * @param maximized true to set maximized, false to set unmaximized
	 */
	virtual void setWindowMaximized(bool maximized = true) override;

	/**
	 * @brief Moves the window to specific screen coordinates
	 * @param x The X screen coordinate
	 * @param y The Y screen coordinate
	 */
	virtual void setWindowPosition(int x, int y) override;

	/**
	 * @brief Asks to create or not a full screen display
	 * @param fullScreen true if full screen is wanted, otherwise false
	 * @warning This method must be called *before* initialization
	 */
	virtual void setFullScreen(bool fullScreen = true) override;

	/**
	 * @brief Asks for a borderless window
	 * @param borderLess true for a borderless, otherwise false
	 */
	virtual void setWindowBorderLess(bool borderLess = true) override;

	/**
	 * @brief Sets the cursor visibility
	 * @param visible true to show the cursor, false to hide it
	 */
	virtual void setCursorVisibility(bool visible) override;

	/**
	 * @brief Freezes the cursor in the window's center
	 * @param warped true to enable, false to disable
	 */
	virtual void setCursorWarped(bool warped) override;

	virtual bool hasPendingEvents() override;
	virtual Event* popEvent() override;

private:
	void initKeyCodes();
	void initButtonCodes();

private:
	template<typename T> static T ext(const String& name)
	{
		return reinterpret_cast<T>(glXGetProcAddressARB(
		    reinterpret_cast<const GLubyte*>(name.toCString())));
	}

private:
	bool         _initiating;
	bool         _movedWindow;
	int          _minGlxMajorVersion;
	int          _minGlxMinorVersion;

	Display*     _display;
	Window       _window;

	Atom         _atomWinClose;
	Atom         _atomWinName;
	Atom         _atomState;
	Atom         _atomStateFullscreen;
	Atom         _atomStateMaxiVert;
	Atom         _atomStateMaxiHoriz;

	GC           _gc;
	XIM          _im;
	XIC          _ic;
	XVisualInfo* _visual;
	GLXWindow    _glxWindow;
	GLXContext   _glxContext;

	Stack<Event*> _eventsQueue;

private:
	static Map<unsigned int, KeyEvent::Key> _keyCodes;
	static Map<unsigned int, MouseButtonEvent::Button> _mouseButtons;
};

} // ns GLE_NS

#endif // GLE_WINDOW_SYSTEM == GLE_X11
#endif // !_GLE_CONTEXTS_X11CONTEXT_H_
