/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief Contexts/GLContext.h
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-04-01
 */

#ifndef _GLE_CONTEXTS_GLCONTEXT_H_
#define _GLE_CONTEXTS_GLCONTEXT_H_

#include "GLE/Utils/String.h"

namespace GLE_NS {

class Event;

/**
 * @class GLContext Headers/GLContext.h <GLContext>
 * @brief An OpenGL's context interface
 */
class GLContext
{
public:
	GLContext();
	virtual ~GLContext();

	/**
	 * @brief Returns a new OpenGL context depending on the platform
	 * @return A new OpenGL context instance
	 */
	static GLContext* create();

	/**
	 * @brief Implements how to initialize the OpenGL context
	 */
	virtual void init() = 0;

	/**
	 * @brief Implements how to swap buffers in a double-buffered framebuffer
	 */
	virtual void swapBuffers() = 0;

	virtual bool hasPendingEvents() = 0;

	virtual Event* popEvent() = 0;

	/**
	 * @brief Returns the OpenGL major version
	 */
	int glMajorVersion() const;

	/**
	 * @brief Returns the OpenGL minor version
	 */
	int glMinorVersion() const;

	/**
	 * @brief Returns whether the context is initialized or not
	 */
	bool isInitialized() const;

	/**
	 * @brief Sets the title of the window to create
	 * @param title The window's title
	 */
	virtual void setWindowTitle(const String& title) = 0;

	/**
	 * @brief Sets the display window's size
	 * @param width The window's width
	 * @param height The window's height
	 * @warning This method must be called *before* initialization
	 */
	virtual void setWindowSize(int width, int height) = 0;

	/**
	 * @brief Sets the window as maximized (takes all available space)
	 * @param maximized true to set maximized, false to set unmaximized
	 */
	virtual void setWindowMaximized(bool maximized = true) = 0;

	/**
	 * @brief Asks to create or not a full screen display
	 * @param fullScreen true if full screen is wanted, otherwise false
	 * @warning This method must be called *before* initialization
	 */
	virtual void setFullScreen(bool fullScreen = true) = 0;

	/**
	 * @brief Moves the window to specific screen coordinates
	 * @param x The X screen coordinate
	 * @param y The Y screen coordinate
	 */
	virtual void setWindowPosition(int x, int y) = 0;

	/**
	 * @brief Asks for a borderless window
	 * @param borderLess true for a borderless, otherwise false
	 */
	virtual void setWindowBorderLess(bool borderLess = true) = 0;

	/**
	 * @brief Sets the cursor visibility
	 * @param visible true to show the cursor, false to hide it
	 */
	virtual void setCursorVisibility(bool visible) = 0;

	/**
	 * @brief Show the cursor
	 */
	void showCursor();

	/**
	 * @brief Hides the cursor
	 */
	void hideCursor();

	/**
	 * @brief Freezes the cursor in the window's center
	 * @param warped true to enable, false to disable
	 */
	virtual void setCursorWarped(bool warped) = 0;

	/**
	 * @brief Freezes the cursor in the windows's center
	 */
	void warpCursor();

	/**
	 * @brief Unfreezes the cursor in the windows's center
	 */
	void unwarpCursor();

	/**
	 * @brief Ask for a double buffering (the default) or not
	 * @param doubleBufferingEnabled Enable or not double buffering
	 */
	void setDoubleBuffering(bool doubleBufferingEnabled);

	/**
	 * @brief Enables or not vertical synchronisation
	 * @param vSync true to enable vertical synchronisation, otherwise false
	 */
	void setVSync(bool vSync);

	/**
	 * @brief Ask for a particular OpenGL version
	 * @param major The major version number
	 * @param minor The minor version number
	 * @warning This method must be called *before* initialization
	 */
	void setOpenGLVersion(int major, int minor);

	/**
	 * @brief Set a required minimal OpenGL version
	 * @param major The major version number
	 * @param minor The minor version number
	 */
	void setMinimalOpenGLVersion(int major, int minor);

	/**
	 * @brief Asks a compatibility profile for OpenGL 3.2 and later
	 * @warning This method must be called *before* initialization
	 */
	void setCompatibilityProfile();

protected:
	bool   _inited;
	bool   _fullScreen;
	bool   _borderLess;
	bool   _cursorVisible;
	bool   _cursorWarped;
	bool   _doubleBuffering;
	bool   _vSync;
	bool   _isMaximized;
	int    _windowWidth;
	int    _windowHeight;
	int    _windowX;
	int    _windowY;
	String _windowTitle;
	int    _glMajorVersion;
	int    _glMinorVersion;
	int    _minGlMajorVersion;
	int    _minGlMinorVersion;
	bool   _compatibilityProfile;

	bool   _keyRepeat;
};

} // ns GLE_NS

#endif // !_GLE_CONTEXTS_GLCONTEXT_H_
