/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief Contexts/Win32Context.h
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-09-21
 */

#ifndef _GLE_CONTEXTS_WIN32CONTEXT_H_
#define _GLE_CONTEXTS_WIN32CONTEXT_H_

#include "GLE/GLEngine.h"

#if GLE_WINDOW_SYSTEM == GLE_WIN32

#include "GLE/Contexts/GLContext.h"

#include <windows.h>

namespace GLE_NS {

class Win32Context : public GLContext
{
public:
	Win32Context();
	virtual ~Win32Context();

	virtual void init();
	virtual void swapBuffers();

private:
	void fatal(const String& msg);
	void createWindow();

private:
	HWND  _window;
	HDC   _dc;
	HGLRC _glContext;
};

} // ns GLE_NS

#endif // GLE_WINDOW_SYSTEM == GLE_WIN32
#endif // !_GLE_CONTEXTS_WIN32CONTEXT_H_
