/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief Objects/Primitives/UVSphere.h
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-09-29
 */

#ifndef _GLE_OBJECTS_PRIMITIVES_UVSPHERE_H_
#define _GLE_OBJECTS_PRIMITIVES_UVSPHERE_H_

#include "GLE/Objects/AbstractMesh.h"

#include "GLE/VertexArray.h"
#include "GLE/Buffer.h"

namespace GLE_NS {
namespace Meshes {

class UVSphere : public AbstractMesh
{
public:
	UVSphere();

private:
	VertexArray _vao;
	ArrayBuffer _vertex;
};

} // ns Meshes
} // ns GLE_NS

#endif // !_GLE_OBJECTS_PRIMITIVES_UVSPHERE_H_
