/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief Objects/Frame.h
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-09-22
 */

#ifndef _GLE_OBJECTS_FRAME_H_
#define _GLE_OBJECTS_FRAME_H_

#include "GLE/Utils/Matrix.h"
#include "GLE/Utils/Vector.h"

namespace GLE_NS {

class Frame
{
public:
	Frame();

	void move(const Vector3& vec);
	void pitch(float angle);
	void yaw(float angle);
	void roll(float angle);

	void setPosition(const Vector3& position);
	void setPitch(float pitch);
	void setYaw(float yaw);
	void setRoll(float roll);

	Matrix4 matrix();
	Vector3 position() const;
	float   pitch() const;
	float   yaw() const;
	float   roll() const;

private:
	virtual void updateMatrix();

protected:
	bool    _isUpToDate;
	Matrix4 _matrix;
	Vector3 _position;
	float   _pitch;
	float   _yaw;
	float   _roll;
};

} // ns GLE_NS

#endif // !_GLE_OBJECTS_FRAME_H_
