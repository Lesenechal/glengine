/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief GLEngine.h
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2012-03-27
 */

#ifndef _GLE_GLENGINE_H_
#define _GLE_GLENGINE_H_

#if __cplusplus > 199711L
#  define GLE_CPP11 1
#else
#  define GLE_CPP11 0
#endif

#define GLE_W32 1
#define GLE_CGL 2
#define GLE_X11 3

#if defined(_WIN32)
#  define GLE_WINDOW_SYSTEM GLE_W32
#elif defined(__OSX__)
#  define GLE_WINDOW_SYSTEM GLE_CGL
#else
#  define GLE_WINDOW_SYSTEM GLE_X11
#endif

#ifndef GLE_NS
#  define GLE_NS GLE
#endif // !GLE_NS

#include <cmath>

#ifndef M_PI
#  define M_PI 3.14159265358979323846
#endif // !M_PI

#define DEGRAD(degrees) ((degrees) / 180.0f * M_PI)

#endif // !_GLE_GLENGINE_H_
