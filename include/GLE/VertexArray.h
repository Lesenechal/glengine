/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief VertexArray.h
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-09-22
 */

#ifndef _GLE_VERTEXARRAY_H_
#define _GLE_VERTEXARRAY_H_

#include "GLE/GLObject.h"

namespace GLE_NS {

class Buffer;

class VertexArray : public GLObject
{
public:
	enum DrawMode
	{
		Points                 = GL_POINTS,
		LineStrip              = GL_LINE_STRIP,
		LineLoop               = GL_LINE_LOOP,
		Lines                  = GL_LINES,
		LineStripAdjacency     = GL_LINE_STRIP_ADJACENCY,
		LinesAdjacency         = GL_LINES_ADJACENCY,
		TriangleStrip          = GL_TRIANGLE_STRIP,
		TriangleFan            = GL_TRIANGLE_FAN,
		Triangles              = GL_TRIANGLES,
		TriangleStripAdjacency = GL_TRIANGLE_STRIP_ADJACENCY,
		TrianglesAdjacency     = GL_TRIANGLES_ADJACENCY,
		Patches                = GL_PATCHES
	};

	VertexArray();

	void setBuffer(int index,
	               Buffer* buffer,
	               int componentsCount,
	               bool normalize = false);

	void draw(DrawMode mode, int start = 0, int count = -1);

	virtual void bind() override;

private:
	int _count;
};

} // ns GLE_NS

#endif // !_GLE_VERTEXARRAY_H_
