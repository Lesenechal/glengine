/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief StackedMatrix.inl.cpp
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-09-21
 */

#include "GLE/GLEngine.h"

namespace GLE_NS {

template<typename T, int W, int H>
StackedMatrix<T, W, H>::StackedMatrix()
{}

template<typename T, int W, int H>
void StackedMatrix<T, W, H>::push()
{
	_stack.push(*this);
}

template<typename T, int W, int H>
void StackedMatrix<T, W, H>::pop()
{
	*reinterpret_cast<Matrix<T, W, H>*>(this) = _stack.pop();
}

} // ns GLE_NS
