/***************************************************************************
 * Copyright © 2015 Kévin Lesénéchal <kevin.lesenechal@gmail.com>          *
 *                                                                         *
 * This file is part of GLEngine, see <http://glengine.lesenechal.org/>    *
 *                                                                         *
 * GLEngine is free software: you can redistribute it and/or modify it     *
 * under the terms of the GNU Lesser General Public License as published   *
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * GLEngine is distributed in the hope that it will be useful, but         *
 * WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Lesser General Public License for more details.                     *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with GLEngine. If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

/**
 * @brief Events/KeyEvent.h
 * @author Kévin Lesénéchal <kevin@lesenechal.org>
 * @date 2013-09-28
 */

#ifndef _GLE_EVENTS_KEYEVENT_H_
#define _GLE_EVENTS_KEYEVENT_H_

#include "GLE/Events/Event.h"

#include "GLE/Utils/String.h"

namespace GLE_NS {

class KeyEvent : public Event
{
public:
	enum Key
	{
		Unknown,
		A, B, C, D, E, F, G, H, I, J, K, L, M,
		N, O, P, Q, R, S, T, U, V, W, X, Y, Z,
		Num0, Num1, Num2, Num3, Num4, Num5, Num6, Num7, Num8, Num9,
		BackQuote, Hyphen, Equal, Backspace,
		Tab, LeftBracket, RightBracket, Backslash,
		CapsLock, SemiColon, Apostrophe, Enter,
		LeftShift, ISOExtraLeft, Comma, Period, Slash, RightShift,
		LeftCtrl, LeftMeta, LeftAlt, Space,
		AltGr, RightMeta, ContextMenu, RightCtrl,
		Escape, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12,
		Left, Right, Up, Down,
		Insert, Delete, Home, End, PageUp, PageDown,
		NumPad0, NumPad1, NumPad2, NumPad3, NumPad4,
		NumPad5, NumPad6, NumPad7, NumPad8, NumPad9,
		NumPadSlash, NumPadAsterisk, NumPadHyphen,
		NumPadPlus, NumPadEnter, NumPadPeriod
	};

	KeyEvent(Type type, Key key, const String& text);
	virtual ~KeyEvent() override {}

	Key    key() const;
	String text() const;

protected:
	Key    _key;
	String _text;
};

class KeyPressEvent : public KeyEvent
{
public:
	KeyPressEvent(Key key, const String& text)
	  : KeyEvent(Event::KeyPressEvent, key, text)
	{}
	virtual ~KeyPressEvent() override {}
};

class KeyReleaseEvent : public KeyEvent
{
public:
	KeyReleaseEvent(Key key, const String& text)
	  : KeyEvent(Event::KeyReleaseEvent, key, text)
	{}
	virtual ~KeyReleaseEvent() override {}
};

} // ns GLE_NS

#endif // !_GLE_EVENTS_KEYEVENT_H_
